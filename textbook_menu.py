from PyQt5.QtCore import *
from PyQt5.QtWidgets import *


class TextbookMenu(QWidget):
    main_menu_button_clicked = pyqtSignal()
    # True if studying a particular section, False if otherwise.
    study_desired = pyqtSignal(bool)

    class TextbookLeftMenu(QWidget):
        def __init__(self, parent):
            super(TextbookMenu.TextbookLeftMenu, self).__init__(parent)

            # Widgets:
            self.add_chapter_button = QPushButton('Add Chapter')
            self.add_section_button = QPushButton('Add Section')

            view_calendar_button = QPushButton('View Calendar')
            view_calendar_button.setDisabled(True)

            note_label = QLabel('Notes')
            note = QPlainTextEdit()
            note_label.setBuddy(note)

            main_menu_button = QPushButton('Main Menu')
            textbook_settings_button = QPushButton('Textbook settings')
            textbook_settings_button.setDisabled(True)

            # Layout:
            layout = QVBoxLayout()
            layout.addWidget(self.add_chapter_button)
            layout.addWidget(self.add_section_button)
            layout.addWidget(view_calendar_button)
            layout.addStretch(200)
            layout.addSpacing(100)
            layout.addWidget(note_label)
            layout.addWidget(note)
            layout.addWidget(main_menu_button)
            layout.addWidget(textbook_settings_button)
            self.setLayout(layout)

            # Connections
            main_menu_button.clicked.connect(lambda: parent.main_menu_button_clicked.emit())

    class ContentMenu(QWidget):
        def __init__(self, parent):
            super(TextbookMenu.ContentMenu, self).__init__(parent)
            self.chapter_buttons = {}

    class ChapterButton(QPushButton):
        chapter_button_clicked = pyqtSignal(int)

        def __init__(self, parent, chapter_id, text):
            super(TextbookMenu.ChapterButton, self).__init__(parent)
            self.chapter_id = chapter_id
            self.setText(text)
            self.clicked.connect(lambda: self.chapter_button_clicked.emit(self.chapter_id))

    def update_layout(self, textbook_contents, current_chapter):
        self.textbook_content_menu = self.ContentMenu(self)
        layout = QVBoxLayout()
        for i in range(len(textbook_contents)):
            chapter_name = list(textbook_contents.keys())[i]
            chapter_button = self.ChapterButton(self, i, chapter_name)
            chapter_button.setMinimumWidth(398)
            layout.addWidget(chapter_button)

            self.textbook_content_menu.chapter_buttons[chapter_button] = None

            if current_chapter == chapter_name:
                section_holder, buttons = self.build_section_holder(chapter_name, textbook_contents)

                layout.addWidget(section_holder)
                self.textbook_content_menu.chapter_buttons[chapter_button] = buttons

        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)
        self.textbook_content_menu.setLayout(layout)
        self.content_menu_holder.setWidget(self.textbook_content_menu)

    @staticmethod
    def build_section_holder(chapter_name, textbook_contents):
        section_holder = QWidget()
        section_layout = QVBoxLayout()
        section_layout.setSpacing(0)
        section_layout.setContentsMargins(0, 0, 0, 0)
        buttons = []
        for section_name in textbook_contents[chapter_name]:
            section_button = QPushButton(section_name)
            section_button.setMinimumWidth(398)
            section_button.setStyleSheet('background:blue;')
            section_layout.addWidget(section_button)
            buttons.append(section_button)

        section_holder.setLayout(section_layout)
        return section_holder, buttons

    class RightMenu(QWidget):
        def __init__(self, parent):
            super(TextbookMenu.RightMenu, self).__init__(parent)

            # Widgets:
            sections_read_label = QLabel('Sections Read')
            section_read = QProgressBar()
            problems_finished_label = QLabel('Problems Finished')
            problems_finished = QProgressBar()
            mastery_of_problems_label = QLabel('Mastery of Problems')
            mastery_of_problems = QProgressBar()

            self.add_content_button = QPushButton('Add Content to <Section | Chapter>')

            new_problems_label = QLabel('New Problems')
            new_problems_box = QLineEdit()
            new_problems_box.setDisabled(True)
            new_problems_layout = QHBoxLayout()
            new_problems_layout.addWidget(new_problems_label)
            new_problems_layout.addWidget(new_problems_box)

            review_problems_label = QLabel('Review Problems')
            review_problems_box = QLineEdit()
            review_problems_box.setDisabled(True)
            review_problems_layout = QHBoxLayout()
            review_problems_layout.addWidget(review_problems_label)
            review_problems_layout.addWidget(review_problems_box)

            study_button = QPushButton('Study')
            study_button.clicked.connect(lambda: parent.study_desired.emit(False))
            study_section_button = QPushButton('Study Section')
            study_section_button.clicked.connect(lambda: parent.study_desired.emit(True))

            # Layout:
            layout = QVBoxLayout()
            layout.addWidget(sections_read_label)
            layout.addWidget(section_read)
            layout.addWidget(problems_finished_label)
            layout.addWidget(problems_finished)
            layout.addWidget(mastery_of_problems_label)
            layout.addWidget(mastery_of_problems)
            layout.addStretch(300)
            layout.addSpacing(100)
            layout.addWidget(self.add_content_button)
            layout.addLayout(new_problems_layout)
            layout.addLayout(review_problems_layout)
            layout.addWidget(study_button)
            layout.addWidget(study_section_button)
            self.setLayout(layout)

    def __init__(self, parent):
        super(TextbookMenu, self).__init__(parent)

        # Widgets:
        self.left_menu = self.TextbookLeftMenu(self)
        self.textbook_content_menu = self.ContentMenu(self)
        self.content_menu_holder = QScrollArea()
        self.content_menu_holder.setFixedWidth(400)

        self.content_menu_holder.setWidget(self.textbook_content_menu)
        self.right_menu = self.RightMenu(self)

        # Layout:
        layout = QHBoxLayout()
        layout.addWidget(self.left_menu)
        layout.addWidget(self.content_menu_holder)
        layout.addWidget(self.right_menu)
        self.setLayout(layout)
