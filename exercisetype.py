from enum import Enum


class ExerciseType(Enum):
    PROBLEM = 0
    LECTURE_TOPIC = 1
    FACT = 2
    THEOREM = 3
    EXAMPLE = 4
    DEFINITION = 5
    PROOF = 6
