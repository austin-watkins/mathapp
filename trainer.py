import collections
import sqlite3
from PyQt5.QtCore import pyqtSignal, QObject
from PyQt5.QtWidgets import QApplication
import time
from datetime import datetime

from exercisetype import ExerciseType
from interaction_type import InteractionType
import database
import planner as pl


class Trainer(QObject):
    display_bookshelf = pyqtSignal(list)
    display_textbook_contents = pyqtSignal(collections.OrderedDict, str)
    display_chapter_problems = pyqtSignal(list)
    display_exercise_content = pyqtSignal(dict, ExerciseType, bool)
    display_problem = pyqtSignal(str)
    display_solution = pyqtSignal(str)
    study_session_over = pyqtSignal()

    def __init__(self):
        super(Trainer, self).__init__()

        self._current_textbook = None
        self._current_chapter = None
        self._current_section = None
        self._current_exercise = None
        self._planner = None

        self._sessions_problems = None

        self._start_of_problem = None
        self.time_taken = 0

    def show_solution_clicked(self):
        exercise_content = database.Exercise.get_content(self._current_textbook, self._current_chapter,
                                                         self._current_section, self._current_exercise)

        self.time_taken = time.time() - self._start_of_problem
        print(f'time taken {time.time() - self._start_of_problem}')
        if len(exercise_content) != 1:
            self.display_solution.emit(exercise_content['solution'])

    def success_button_clicked(self):
        ending = InteractionType.SUCCESS
        duration = self.time_taken
        date = datetime.now()

        interaction = {'ending': ending, 'duration': duration, 'date': date}

        database.Chapter.insert_new_interaction(self._current_textbook, self._current_chapter, self._current_section,
                                                self._current_exercise, interaction)
        self._sessions_problems.pop(0)
        if len(self._sessions_problems) == 0:
            self.study_session_over.emit()
        else:
            self.give_user_problem()

    def failure_button_clicked(self):
        ending = InteractionType.FAILURE
        duration = self.time_taken
        date = datetime.now()

        interaction = {'ending': ending, 'duration': duration, 'date': date}

        database.Chapter.insert_new_interaction(self._current_textbook, self._current_chapter, self._current_section,
                                                self._current_exercise, interaction)
        self._sessions_problems.pop(0)
        if len(self._sessions_problems) == 0:
            self.study_session_over.emit()
        else:
            self.give_user_problem()

    def update_bookshelf(self):
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        textbooks = []
        for x in c.execute('SELECT Name FROM TEXTBOOK'):
            textbooks.append(x[0])
        self.display_bookshelf.emit(textbooks)

        self._current_textbook = None
        self._current_chapter = None
        self._current_section = None

        db.close()

    def prepare_study(self, is_particular_section):
        if is_particular_section is False:
            exercises = database.Exercise.get_textbook_exercises(self._current_textbook)
            interactions = self.get_exercise_interactions(self._current_textbook)
        else:
            exercises = database.Exercise.get_section_exercises(self._current_textbook, self._current_chapter,
                                                                self._current_section)
            section = {'chapter': self._current_chapter, 'section': self._current_section}
            interactions = database.Exercise.get_textbook_exercise_interactions(self._current_textbook, section)


        exercise_settings = self.get_exercise_settings(self._current_textbook)
        mastery_schedule = self.get_mastery_schedule(self._current_textbook)

        planner = pl.Planner(exercises, interactions, exercise_settings, mastery_schedule)
        self._sessions_problems = planner.session_problems

        self.give_user_problem()

    def prepare_study_group(self, group):
        if group is 'math_foundations':
            # 1 - Linear Algebra Done Right
            # 9 - Secrets of Mental Math
            # 13 - Calculus Vol 1
            # 10 - Discrete Mathematics and Its Applications
            # 16 - Basic Complex Analysis
            # 17 - Topology
            # 18 - Physics for sci. and eng. a strategic approach
            # 19 - Probability and Random Processes
            # textbooks = [1, 9, 10, 13, 17, 16]
            # textbooks = [18, 17, 16]
            textbooks = [19, 20, 21, 22, 23]

        exercises = list()
        interactions = list()
        for book in textbooks:
            book_exercises = database.Exercise.get_textbook_exercises(book)
            exercises += book_exercises

            book_interactions = self.get_exercise_interactions(book)
            interactions += book_interactions

        # fixme: Don't hard code exercise settings and mastering schedule
        exercise_settings = self.get_exercise_settings(12)
        mastery_schedule = self.get_mastery_schedule(12)

        planner = pl.Planner(exercises, interactions, exercise_settings, mastery_schedule)
        self._sessions_problems = planner.session_problems

        self.give_user_problem()

    def give_user_problem(self):
        if len(self._sessions_problems) == 0:
            self.study_session_over.emit()
            return
        next_exercise = self._sessions_problems[0]
        self._current_textbook = next_exercise[0]
        self._current_chapter = next_exercise[1]
        self._current_section = next_exercise[2]
        self._current_exercise = next_exercise[3]
        exercise_content = database.Exercise.get_content(self._current_textbook, self._current_chapter,
                                                         self._current_section, self._current_exercise)
        self.display_problem.emit(exercise_content['problem'])
        self._start_of_problem = time.time()

    def add_textbook(self, settings):
        database.Textbook.record_new_textbook(settings)
        self.update_bookshelf()

    def add_chapter(self, chapter_info):
        database.Chapter.insert_new_chapter(chapter_info, self._current_textbook)
        self.refresh_textbook_contents()

    def refresh_textbook_contents(self):
        chapter_table = database.Chapter.get_chapters(self._current_textbook)
        chapter_table = database.Section.get_sections(self._current_textbook, chapter_table)
        current_chapter_name = database.Chapter.get_chapter_name(self._current_textbook, self._current_chapter)
        self.display_textbook_contents.emit(chapter_table, current_chapter_name)

    def add_section(self, section_info):
        database.Section.insert_new_section(section_info, self._current_textbook, self._current_chapter)
        self.refresh_textbook_contents()
        print('current_textbook is', self._current_chapter)

    def open_textbook(self, textbook_id):
        self._current_textbook = textbook_id
        self._current_chapter = 0
        if database.Chapter.get_new_chapter_id(self._current_textbook) > 0:
            self.refresh_textbook_contents()
        else:
            self.display_textbook_contents.emit(collections.OrderedDict(), None)

        print('current textbook is: ', self._current_textbook)

    def chapter_button_clicked(self, chapter_id):
        self._current_chapter = chapter_id
        self._current_section = None
        self._current_exercise = None
        self.refresh_textbook_contents()
        print('Current chapter is: ', chapter_id)

    def section_button_clicked(self):
        self._current_exercise = None
        button_text = QApplication.sender(self).text()
        self._current_section = database.Section.get_section_id(self._current_textbook, self._current_chapter,
                                                                button_text)
        print('Current section is: ', self._current_section)

    def exercise_button_clicked(self, exercise_id):
        self._current_exercise = exercise_id
        exercise_content = database.Exercise.get_content(self._current_textbook, self._current_chapter,
                                                         self._current_section, self._current_exercise)
        exercise_type = database.Exercise.get_exercise_type(self._current_textbook, self._current_chapter,
                                                            self._current_section, self._current_exercise)
        exercise_is_activated = database.Exercise.get_activated(self._current_textbook, self._current_chapter,
                                                                self._current_section, self._current_exercise)

        self.display_exercise_content.emit(exercise_content, exercise_type, exercise_is_activated)

    def add_content_button_clicked(self):
        self._current_exercise = 0
        self._refresh_chapter_exercises()
        print('add_content_button_clicked: ran')

    def _refresh_chapter_exercises(self):
        exercise_table = database.Exercise.get_section_exercises(self._current_textbook, self._current_chapter,
                                                                 self._current_section, activated=False)
        self.display_chapter_problems.emit(exercise_table)

    def delete_exercise(self):
        database.Exercise.delete_exercise(self._current_textbook, self._current_chapter, self._current_section,
                                          self._current_exercise)
        self._refresh_chapter_exercises()

    def activate_exercise(self, is_checked: bool) -> None:
        """
        If an exercise exists then change the activation, if it doesn't then don't.

        I'm not sure that this is the best way to do this. But there is an argument to be made for this method.
        That is, any other method would probably have to have logic on the front end to handle the situation in which
        a problem doesn't exist. I don't think that there should be logic on the front end.

        The thing that I don't like about this approach is that it makes it more difficult to debug.

        :param is_checked: If the checkbox Activated is checked
        """
        problem_exists = database.Exercise.exercise_exists(self._current_textbook, self._current_chapter,
                                                           self._current_section,
                                                           self._current_exercise)
        if problem_exists:
            database.Exercise.set_activated(self._current_textbook, self._current_chapter, self._current_section,
                                            self._current_exercise, is_checked)

    def add_exercise(self, exercise_type, data):
        if exercise_type in (ExerciseType.PROBLEM, ExerciseType.EXAMPLE, ExerciseType.PROOF):
            database.Exercise.insert_problem(self._current_textbook, self._current_chapter, self._current_section,
                                             exercise_type, data)
        elif exercise_type in (ExerciseType.DEFINITION, ExerciseType.THEOREM, ExerciseType.FACT):
            database.Exercise.insert_definition(self._current_textbook, self._current_chapter, self._current_section,
                                                exercise_type, data)
        elif exercise_type is ExerciseType.LECTURE_TOPIC:
            database.Exercise.insert_topic(self._current_textbook, self._current_chapter, self._current_section,
                                           exercise_type, data)
        else:
            raise ValueError("Exercise type not supported in trainer.add_exercise")
        self._refresh_chapter_exercises()

    @staticmethod
    def get_exercise_interactions(textbook_id):
        interactions = database.Exercise.get_textbook_exercise_interactions(textbook_id)
        return interactions

    @staticmethod
    def get_exercise_settings(textbook_id):
        def process_settings(settings):
            """
            Given a list of tuples representing a textbook's exercise settings tranform it to a dict that contains
            the same data with structure dict[exercise_name]["fail" | "review"] = Int
            :param settings: a list of exercise settings of form (exercise_name, days_till_review, days_after_fail)
            :return: A dictionary of dictionaries that contains the days after a failure or review.
            """
            names = {row[0] for row in settings}
            result = {a_name: dict() for a_name in names}
            for row in settings:
                exercise_name = row[0]
                result[exercise_name]['review_counter'] = row[1]
                result[exercise_name]['failure_counter'] = row[2]
            return result

        exercise_settings = database.Textbook.get_exercise_settings(textbook_id)
        exercise_settings = process_settings(exercise_settings)

        return exercise_settings

    @staticmethod
    def get_mastery_schedule(textbook_id):
        def process_schedule(schedule):
            """
            Given a list of tuples that is a mastery schedule transform it to a dict that contains the same data.
            With structure dict[exercise_name] = [list of practice durations].
            :param schedule: a list of tuples representing a mastery schedule.
            :return: dictionary that is mastery schedule.
            """
            # Sorts by steps so duration is monotonically increasing (this is probably unneeded).
            schedule.sort(key=lambda step: step[2])
            exercise_names = {mastery_step[0] for mastery_step in schedule}
            result = {name: [] for name in exercise_names}
            for a_exercise_step in schedule:
                result[a_exercise_step[0]].append(a_exercise_step[2])
            return result

        mastery_schedule = database.Textbook.get_mastery_schedule(textbook_id)
        mastery_schedule = process_schedule(mastery_schedule)
        return mastery_schedule
