from PyQt5.QtWidgets import QWidget, QPushButton, QTextEdit, QLabel, QVBoxLayout, QHBoxLayout, QScrollArea
from PyQt5.QtCore import *

from ast import literal_eval


class Solver(QWidget):
    return_to_textbook = pyqtSignal()
    show_solution = pyqtSignal()
    success_button_clicked = pyqtSignal()
    failure_button_clicked = pyqtSignal()
    clear_display = pyqtSignal()

    def __init__(self, parent):
        super(Solver, self).__init__(parent)

        # Widget:
        menu = Menu(self)
        self._display = ExerciseDisplay(self)
        solver_holder = QScrollArea()
        solver_holder.setWidget(self._display)
        solver_holder.setFixedWidth(1000)

        # Layout:
        layout = QHBoxLayout()
        layout.addWidget(menu)
        layout.addWidget(solver_holder)
        self.setLayout(layout)

        menu.show_solution.connect(self.show_solution)
        menu.success_button_clicked.connect(self.success_button_clicked)
        menu.failure_button_clicked.connect(self.failure_button_clicked)
        self.clear_display.connect(self._display.clear_display)

    def display_problem(self, content):
        self.clear_display.emit()
        content = literal_eval(content)['html']
        self._display.problem.setHtml(content)

    def display_solution(self, content):
        content = literal_eval(content)['html']
        self._display.solution.setHtml(content)


class Menu(QWidget):
    show_solution = pyqtSignal()
    success_button_clicked = pyqtSignal()
    failure_button_clicked = pyqtSignal()

    def __init__(self, parent):
        super(Menu, self).__init__(parent)

        # Widgets:
        solution_button = QPushButton('See Solution')
        try_again_button = QPushButton('Attempt Later')
        self._success_button = QPushButton('Correct')
        self._failure_button = QPushButton('Incorrect')
        return_button = QPushButton('Return to Textbook')
        return_button.clicked.connect(parent.return_to_textbook)
        notes = QTextEdit()

        try_again_button.setDisabled(True)
        self._success_button.setDisabled(True)
        self._failure_button.setDisabled(True)
        notes.setDisabled(True)

        solution_button.clicked.connect(self._solution_button_clicked)
        self._success_button.clicked.connect(self._success_button_handler)
        self._failure_button.clicked.connect(self._fail_button_handler)

        # Layout:
        layout = QVBoxLayout()
        layout.addWidget(solution_button)
        layout.addWidget(try_again_button)
        layout.addWidget(self._success_button)
        layout.addWidget(self._failure_button)
        layout.addStretch(200)
        layout.addSpacing(100)
        layout.addWidget(notes)
        layout.addWidget(return_button)
        self.setLayout(layout)

    def _solution_button_clicked(self):
        self._success_button.setDisabled(False)
        self._failure_button.setDisabled(False)
        self.show_solution.emit()

    def _fail_button_handler(self):
        self.failure_button_clicked.emit()
        self._success_button.setDisabled(True)
        self._failure_button.setDisabled(True)

    def _success_button_handler(self):
        self.success_button_clicked.emit()
        self._success_button.setDisabled(True)
        self._failure_button.setDisabled(True)


class ExerciseDisplay(QWidget):
    def __init__(self, parent):
        super(ExerciseDisplay, self).__init__(parent)

        # Widgets:
        problem_label = QLabel('Problem:')
        self.problem = QTextEdit()
        solution_label = QLabel('Solution:')
        self.solution = QTextEdit()

        self.setFixedHeight(798)
        problem_label.setAlignment(Qt.AlignCenter)
        solution_label.setAlignment(Qt.AlignCenter)
        problem_label.setFixedWidth(1000)
        self.problem.setFixedWidth(1000)
        self.problem.setMinimumHeight(300)
        solution_label.setFixedWidth(1000)
        self.solution.setFixedWidth(1000)
        self.solution.setMinimumHeight(300)

        # Layout:
        layout = QVBoxLayout()
        layout.setContentsMargins(0, 5, 0, 0)
        layout.addWidget(problem_label)
        layout.addWidget(self.problem)
        layout.addWidget(solution_label)
        layout.addWidget(self.solution)
        self.setLayout(layout)

    def clear_display(self):
        self.problem.clear()
        self.solution.clear()
