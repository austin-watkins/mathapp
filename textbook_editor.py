from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

from exercisetype import ExerciseType
from exercise_holder import ProblemHolder, FactHolder, TopicHolder


class TextbookEditor(QWidget):
    exercise_chosen = pyqtSignal(ExerciseType, dict)
    exercise_delete = pyqtSignal()
    exercise_activate = pyqtSignal(bool)

    def __init__(self, parent):
        super(TextbookEditor, self).__init__(parent)

        self.exercise_buttons = []

        # Widgets:
        self.menu = self.Menu(self)
        self._textbook_selector = self.ExerciseSelector(self)
        self._textbook_selector_holder = QScrollArea()
        self._textbook_selector_holder.setFixedWidth(102)
        self._textbook_selector_holder.setWidget(self._textbook_selector)

        self.exercise_display = QStackedWidget()

        self.exercise_display.setCurrentIndex(0)
        self.exercise_display.setFixedWidth(1000)
        self.exercise_display.setFixedHeight(800)

        # Layout:
        layout = QHBoxLayout()
        layout.addWidget(self.menu)
        layout.addWidget(self._textbook_selector_holder)
        layout.addWidget(self.exercise_display)
        self.setLayout(layout)

        # Connections:
        self.type_value_to_index = dict()
        self.form_panel_connections(ProblemHolder(self, ExerciseType.PROBLEM), ExerciseType.PROBLEM, self.menu.add_problem_button)
        self.form_panel_connections(ProblemHolder(self, ExerciseType.EXAMPLE), ExerciseType.EXAMPLE, self.menu.add_example_button)
        self.form_panel_connections(ProblemHolder(self, ExerciseType.PROOF), ExerciseType.PROOF, self.menu.add_proof_button)
        self.form_panel_connections(FactHolder(self, ExerciseType.DEFINITION), ExerciseType.DEFINITION, self.menu.add_definition_button)
        self.form_panel_connections(FactHolder(self, ExerciseType.THEOREM), ExerciseType.THEOREM, self.menu.add_theorem_button)
        self.form_panel_connections(FactHolder(self, ExerciseType.FACT), ExerciseType.FACT, self.menu.add_fact_button)
        self.form_panel_connections(TopicHolder(self, ExerciseType.LECTURE_TOPIC), ExerciseType.LECTURE_TOPIC, self.menu.add_lecture_button)

    def form_panel_connections(self, panel, exercise_type, activation_button):
        index = self.exercise_display.count()
        self.exercise_display.addWidget(panel)
        panel.add_exercise_clicked.connect(self._add_exercise_handler)
        activation_button.clicked.connect(lambda: self._select_exercise_button_clicked_handler(exercise_type))
        panel.delete_exercise_clicked.connect(self._delete_exercise_handler)
        panel.activated_checkbox_clicked.connect(lambda is_activated: self.exercise_activate.emit(is_activated))

        self.type_value_to_index[exercise_type.value] = index

    def _delete_exercise_handler(self):
        self.exercise_delete.emit()
        self.exercise_display.currentWidget().clear_data()

    def _add_exercise_handler(self, exercise_type):
        data = self.exercise_display.currentWidget().get_data()
        self.exercise_chosen.emit(exercise_type, data)

    class Menu(QWidget):
        def __init__(self, parent):
            super(TextbookEditor.Menu, self).__init__(parent)

            # Widgets:
            self.add_example_button = QPushButton('Add Example')
            self.add_definition_button = QPushButton('Add Definition')
            self.add_proof_button = QPushButton('Add Proof')
            self.add_lecture_button = QPushButton('Add Lecture Topic')
            self.add_theorem_button = QPushButton('Add Theorem')
            self.add_fact_button = QPushButton('Add Fact')
            self.add_problem_button = QPushButton('Add Problem')

            note_label = QLabel('Notes')
            note_input = QTextEdit()
            note_label.setBuddy(note_input)

            self.return_to_textbook_button = QPushButton('Return to Textbook')
            self.section_settings_button = QPushButton('Section Settings')

            # Layout:
            layout = QVBoxLayout()
            layout.addWidget(self.add_example_button)
            layout.addWidget(self.add_definition_button)
            layout.addWidget(self.add_proof_button)
            layout.addWidget(self.add_lecture_button)
            layout.addWidget(self.add_theorem_button)
            layout.addWidget(self.add_fact_button)
            layout.addWidget(self.add_problem_button)
            layout.addWidget(note_label)
            layout.addWidget(note_input)
            layout.addWidget(self.return_to_textbook_button)
            layout.addWidget(self.section_settings_button)
            self.setLayout(layout)

    class ExerciseSelector(QWidget):
        def __init__(self, parent):
            super(TextbookEditor.ExerciseSelector, self).__init__(parent)
            self.setFixedWidth(100)

    class ExerciseButton(QPushButton):
        exercise_button_clicked = pyqtSignal(int)

        def button_clicked_helper(self):
            self.exercise_button_clicked.emit(self.exercise_id)

        def __init__(self, parent, exercise_id, exercise_type):
            super(TextbookEditor.ExerciseButton, self).__init__(parent)
            self.exercise_id = exercise_id
            button_title = ExerciseType(exercise_type).name + ' ' + str(exercise_id)
            self.setText(button_title)
            self.clicked.connect(self.button_clicked_helper)

    def add_exercises(self, exercises):

        self._textbook_selector = self.ExerciseSelector(self)

        layout = QVBoxLayout()
        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)

        for exercise in exercises:
            button = self.ExerciseButton(self, exercise[3], int(exercise[4]))
            layout.addWidget(button)
            self.exercise_buttons.append(button)

        self._textbook_selector.setLayout(layout)
        self._textbook_selector_holder.setWidget(self._textbook_selector)

        print('add_exercises_called')

    def _select_exercise_button_clicked_handler(self, exercise_type):
        try:
            if self.exercise_display.currentIndex() == self.type_value_to_index[exercise_type.value]:
                return
            self.exercise_display.currentWidget().clear_data()
            self.exercise_display.setCurrentIndex(self.type_value_to_index[exercise_type.value])
        except KeyError:
            raise ValueError(f'{exercise_type}: not supported')

    def display_exercise(self, exercise_content, exercise_type, is_activated):
        self._select_exercise_button_clicked_handler(exercise_type)
        self.exercise_display.currentWidget().clear_data()
        self.exercise_display.currentWidget().update_content(exercise_content)
        self.exercise_display.currentWidget()._is_activated_checkbox.setChecked(is_activated)











