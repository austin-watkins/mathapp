from PyQt5.QtCore import *
from PyQt5.QtWidgets import *


class TextbookDisplay(QWidget):
    textbook_clicked = pyqtSignal(int)

    def __init__(self, textbook_id, name, parent=None):
        super(TextbookDisplay, self).__init__(parent)
        self.parent = parent
        self._textbook_id = textbook_id

        self.setFixedHeight(443)
        self.setMinimumWidth(350)

        # Widgets:
        self._textbook_image = QLabel()
        self._textbook_image.setFixedWidth(313)
        self._textbook_image.setFixedHeight(398)
        self._textbook_image.setFrameStyle(QFrame.StyledPanel)  # not sure about this

        self._textbook_progress = QProgressBar()
        self._textbook_progress.setFixedHeight(20)
        self._textbook_progress.setFixedWidth(313)
        self._textbook_progress.setValue(20)

        self._textbook_name = QLabel(name)
        self._textbook_name.setFixedWidth(313)
        self._textbook_name.setFixedHeight(25)
        self._textbook_name.setAlignment(Qt.AlignCenter)

        # Layout:
        layout = QVBoxLayout()
        layout.setSpacing(0)
        layout.addWidget(self._textbook_image)
        layout.addWidget(self._textbook_progress)
        layout.addWidget(self._textbook_name)
        self.setLayout(layout)

    def mousePressEvent(self, mouse_event):
        self.textbook_clicked.emit(self._textbook_id)


class Bookshelf(QWidget):
    def __init__(self, parent):
        super(Bookshelf, self).__init__(parent)
        self.textbooks = []
        self.layout = QGridLayout()
        self.setLayout(self.layout)

    def add_books(self, titles):
        for i in range(len(titles)):
            textbook = TextbookDisplay(i, titles[i])
            self.layout.addWidget(textbook, int(i / 4), i % 4)
            self.textbooks.append(textbook)


class MainMenu(QWidget):
    def __init__(self, parent):
        super(MainMenu, self).__init__(parent)

        self.menu = self.ControlPanel(self)
        self.bookshelf = Bookshelf(self)

        scroll_area = QScrollArea()
        scroll_area.setWidgetResizable(True)

        layout = QHBoxLayout()
        self.setLayout(layout)

        scroll_area.setWidget(self.bookshelf)
        
        layout.addWidget(self.menu)
        layout.addWidget(scroll_area)

    class ControlPanel(QWidget):
        def __init__(self, parent):
            super(MainMenu.ControlPanel, self).__init__(parent)

            self.add_textbook_button = QPushButton("Add Textbook")
            self.study_math_foundations = QPushButton("Study Math Foundations")


            side_menu_layout = QVBoxLayout()
            side_menu_layout.addWidget(self.add_textbook_button)
            side_menu_layout.addWidget(self.study_math_foundations)
            self.setLayout(side_menu_layout)
