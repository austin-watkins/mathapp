from unittest import TestCase
import pandas as pd
from datetime import datetime, timedelta
from planner import Planner
from interaction_type import InteractionType
from exercisetype import ExerciseType


def make_exercise(interactions=None):
    """
    A helper function used for testing eligibility into review, mastery, fail, and new.
    If no interactions are given then forms an exercise with no interactions.
    :param interactions: a list of tuples as (ending, hint_activated, date)
    :return: A DataFrame for an exercise
    """

    if interactions is None:
        pass
        # make empty DataFrame exercise

    number_of_interactions = len(interactions)

    interaction_numbers = range(number_of_interactions)
    interaction_type = [x[0].value for x in interactions]
    dates = [x[2] for x in interactions]
    exercise_type = [x[3].value for x in interactions]

    values = [interaction_numbers, interaction_type,  dates, exercise_type]
    columns = ['interaction', 'ending',  'dateTime', 'type']
    unique_df_columns = {column: value for column, value in zip(columns, values)}

    df = pd.DataFrame(unique_df_columns)
    df['chapter'] = 0
    df['section'] = 0
    df['exercise'] = 0
    df['duration'] = 42

    return df


def split_exercise(exercise_interactions):
    """
    Splits an exercise into a single exercise and it's interactions to pass to Planner.
    :param exercise_interactions: A DataFrame exercise the planner needs.
    :return: (exercise, interactions)
    """
    columns = ['chapter', 'section', 'exercise', 'type']
    exercise = exercise_interactions[columns]
    exercise = exercise.drop_duplicates()
    columns = ['chapter', 'section', 'exercise', 'interaction', 'ending', 'duration',  'dateTime']
    interactions = exercise_interactions[columns]
    return exercise, interactions


class TestPlanner(TestCase):
    def test_get_review_problems_ready(self):
        mastery_schedule = pd.DataFrame.from_csv('test_content/standard_mastery_schedule.csv')
        exercise_settings = pd.DataFrame.from_csv('test_content/standard_exercise_schedule.csv')

        exercise = make_exercise([
            (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=131), ExerciseType.PROBLEM),
            (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=100), ExerciseType.PROBLEM),
        ])
        planner = Planner(*split_exercise(exercise), exercise_settings, mastery_schedule)
        actual = planner._is_review_ready(exercise)
        self.assertTrue(actual)

        exercise = make_exercise([
            (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=129), ExerciseType.PROBLEM),
            (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=98), ExerciseType.PROBLEM),
        ])
        planner = Planner(*split_exercise(exercise), exercise_settings, mastery_schedule)
        actual = planner._is_review_ready(exercise)
        self.assertFalse(actual)

    def test_get_mastery_problems_ready(self):
        mastery_schedule = pd.DataFrame.from_csv('test_content/standard_mastery_schedule.csv')
        exercise_settings = pd.DataFrame.from_csv('test_content/standard_exercise_schedule.csv')

        exercise = make_exercise([
            (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=1), ExerciseType.PROBLEM),
        ])
        planner = Planner(*split_exercise(exercise), exercise_settings, mastery_schedule)
        actual = planner._is_mastery_ready(exercise)
        self.assertFalse(actual)

        exercise = make_exercise([
            (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=2), ExerciseType.PROBLEM),
        ])
        planner = Planner(*split_exercise(exercise), exercise_settings, mastery_schedule)
        actual = planner._is_mastery_ready(exercise)
        self.assertTrue(actual)

        exercise = make_exercise([
            (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=8), ExerciseType.PROBLEM),
            (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=1), ExerciseType.PROBLEM),
        ])
        planner = Planner(*split_exercise(exercise), exercise_settings, mastery_schedule)
        actual = planner._is_mastery_ready(exercise)
        self.assertFalse(actual)

        exercise = make_exercise([
            (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=14), ExerciseType.PROBLEM),
            (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=7), ExerciseType.PROBLEM),
        ])
        planner = Planner(*split_exercise(exercise), exercise_settings, mastery_schedule)
        actual = planner._is_mastery_ready(exercise)
        self.assertFalse(actual)

        exercise = make_exercise([
            (InteractionType.FAILURE, False, datetime.now() - timedelta(days=23), ExerciseType.PROBLEM),
            (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=1), ExerciseType.PROBLEM),
        ])
        planner = Planner(*split_exercise(exercise), exercise_settings, mastery_schedule)
        actual = planner._is_mastery_ready(exercise)
        self.assertFalse(actual)

        exercise = make_exercise([
            (InteractionType.FAILURE, False, datetime.now() - timedelta(days=23), ExerciseType.PROBLEM),
            (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=2), ExerciseType.PROBLEM),
        ])
        planner = Planner(*split_exercise(exercise), exercise_settings, mastery_schedule)
        actual = planner._is_mastery_ready(exercise)
        self.assertTrue(actual)

        exercise = make_exercise([
            (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=1), ExerciseType.PROBLEM),
            (InteractionType.ATTEMPT, False, datetime.now(), ExerciseType.PROBLEM),
        ])
        planner = Planner(*split_exercise(exercise), exercise_settings, mastery_schedule)
        actual = planner._is_mastery_ready(exercise)
        self.assertFalse(actual)

        exercise = make_exercise([
            (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=2), ExerciseType.PROBLEM),
            (InteractionType.ATTEMPT, False, datetime.now(), ExerciseType.PROBLEM),
        ])
        planner = Planner(*split_exercise(exercise), exercise_settings, mastery_schedule)

        actual = planner._is_mastery_ready(exercise)
        self.assertTrue(actual)

    def test_get_failed_problems_ready(self):
        mastery_schedule = pd.DataFrame.from_csv('test_content/standard_mastery_schedule.csv')
        exercise_settings = pd.DataFrame.from_csv('test_content/standard_exercise_schedule.csv')

        exercise = make_exercise([(InteractionType.ATTEMPT, False, datetime.now() - timedelta(days=8), ExerciseType.PROBLEM),
                                  (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=7), ExerciseType.PROBLEM),
                                  (InteractionType.FAILURE, False, datetime.now() - timedelta(days=2), ExerciseType.PROBLEM),
                                  (InteractionType.FAILURE, False, datetime.now() - timedelta(days=1), ExerciseType.PROBLEM)])
        planner = Planner(*split_exercise(exercise), exercise_settings, mastery_schedule)
        actual = planner._is_failed_ready(exercise)
        self.assertFalse(actual)

        exercise = make_exercise([(InteractionType.ATTEMPT, False, datetime.now() - timedelta(days=8), ExerciseType.PROBLEM),
                                  (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=7), ExerciseType.PROBLEM),
                                  (InteractionType.FAILURE, False, datetime.now() - timedelta(days=3), ExerciseType.PROBLEM),
                                  (InteractionType.FAILURE, False, datetime.now() - timedelta(days=2), ExerciseType.PROBLEM)])
        planner = Planner(*split_exercise(exercise), exercise_settings, mastery_schedule)
        actual = planner._is_failed_ready(exercise)
        self.assertTrue(actual)

        exercise = make_exercise([(InteractionType.ATTEMPT, False, datetime.now() - timedelta(days=8), ExerciseType.PROBLEM),
                                  (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=7), ExerciseType.PROBLEM),
                                  (InteractionType.ATTEMPT, False, datetime.now() - timedelta(days=3), ExerciseType.PROBLEM),
                                  (InteractionType.ATTEMPT, True, datetime.now() - timedelta(days=2), ExerciseType.PROBLEM)])
        planner = Planner(*split_exercise(exercise), exercise_settings, mastery_schedule)
        actual = planner._is_failed_ready(exercise)
        self.assertTrue(actual)

        exercise = make_exercise([(InteractionType.ATTEMPT, False, datetime.now() - timedelta(days=8), ExerciseType.PROBLEM),
                                  (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=7), ExerciseType.PROBLEM),
                                  (InteractionType.ATTEMPT, False, datetime.now() - timedelta(days=3), ExerciseType.PROBLEM),
                                  (InteractionType.ATTEMPT, True, datetime.now() - timedelta(days=1), ExerciseType.PROBLEM)])
        planner = Planner(*split_exercise(exercise), exercise_settings, mastery_schedule)
        actual = planner._is_failed_ready(exercise)
        self.assertFalse(actual)

        exercise = make_exercise([(InteractionType.ATTEMPT, True, datetime.now() - timedelta(days=2), ExerciseType.PROBLEM)])
        planner = Planner(*split_exercise(exercise), exercise_settings, mastery_schedule)
        actual = planner._is_failed_ready(exercise)
        self.assertTrue(actual)

        exercise = make_exercise([(InteractionType.ATTEMPT, True, datetime.now() - timedelta(days=1), ExerciseType.PROBLEM)])
        planner = Planner(*split_exercise(exercise), exercise_settings, mastery_schedule)
        actual = planner._is_failed_ready(exercise)
        self.assertFalse(actual)

    def test_review_eligible(self):
        mastery_schedule = pd.DataFrame.from_csv('test_content/standard_mastery_schedule.csv')
        exercise_settings = pd.DataFrame.from_csv('test_content/standard_exercise_schedule.csv')

        exercise = make_exercise([
            (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=31), ExerciseType.PROBLEM),
            (InteractionType.SUCCESS, False, datetime.now(), ExerciseType.PROBLEM),
        ])
        planner = Planner(*split_exercise(exercise), exercise_settings, mastery_schedule)
        actual = planner._exercises_df.at[0, 'type'] == 'review'
        self.assertTrue(actual)

        exercise = make_exercise([
            (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=29), ExerciseType.PROBLEM),
            (InteractionType.SUCCESS, False, datetime.now(), ExerciseType.PROBLEM),
        ])
        planner = Planner(*split_exercise(exercise), exercise_settings, mastery_schedule)
        actual = planner._is_review_ready(exercise)
        self.assertFalse(actual)

        exercise = make_exercise([
            (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=31), ExerciseType.PROBLEM),
            (InteractionType.SUCCESS, False, datetime.now(), ExerciseType.PROBLEM),
            (InteractionType.ATTEMPT, True, datetime.now(), ExerciseType.PROBLEM),
        ])
        planner = Planner(*split_exercise(exercise), exercise_settings, mastery_schedule)
        actual = planner._is_review_ready(exercise)
        self.assertFalse(actual)

        exercise = make_exercise([
            (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=62), ExerciseType.PROBLEM),
            (InteractionType.SUCCESS, False, datetime.now() - timedelta(days=31), ExerciseType.PROBLEM),
            (InteractionType.FAILURE, False, datetime.now(), ExerciseType.PROBLEM),
            (InteractionType.SUCCESS, False, datetime.now(), ExerciseType.PROBLEM),
        ])
        planner = Planner(*split_exercise(exercise), exercise_settings, mastery_schedule)
        actual = planner._is_review_ready(exercise)
        self.assertFalse(actual)

    def test_mastery_eligible(self):
        exercise = make_exercise([
            (InteractionType.ATTEMPT, False, datetime.now(), ExerciseType.PROBLEM)
        ])
        actual = Planner._mastery_eligible(exercise)
        self.assertFalse(actual)

        exercise = make_exercise([
            (InteractionType.SUCCESS, False, datetime.now(), ExerciseType.PROBLEM),
        ])
        actual = Planner._mastery_eligible(exercise)
        self.assertTrue(actual)

        exercise = make_exercise([
            (InteractionType.SUCCESS, False, datetime.now(), ExerciseType.PROBLEM),
            (InteractionType.SUCCESS, False, datetime.now(), ExerciseType.PROBLEM),
        ])
        actual = Planner._mastery_eligible(exercise)
        self.assertTrue(actual)

        exercise = make_exercise([
            (InteractionType.FAILURE, False, datetime.now(), ExerciseType.PROBLEM),
            (InteractionType.SUCCESS, False, datetime.now(), ExerciseType.PROBLEM),
        ])
        actual = Planner._mastery_eligible(exercise)
        self.assertTrue(actual)

        exercise = make_exercise([
            (InteractionType.SUCCESS, False, datetime.now(), ExerciseType.PROBLEM),
            (InteractionType.FAILURE, False, datetime.now(), ExerciseType.PROBLEM),
        ])
        actual = Planner._mastery_eligible(exercise)
        self.assertFalse(actual)

        exercise = make_exercise([
            (InteractionType.SUCCESS, False, datetime.now(), ExerciseType.PROBLEM),
            (InteractionType.ATTEMPT, True, datetime.now(), ExerciseType.PROBLEM),
        ])
        actual = Planner._mastery_eligible(exercise)
        self.assertFalse(actual)

        exercise = make_exercise([
            (InteractionType.SUCCESS, False, datetime.now(), ExerciseType.PROBLEM),
            (InteractionType.ATTEMPT, False, datetime.now(), ExerciseType.PROBLEM),
        ])
        actual = Planner._mastery_eligible(exercise)
        self.assertTrue(actual)

        exercise = make_exercise([
            (InteractionType.ATTEMPT, False, datetime.now(), ExerciseType.PROBLEM),
            (InteractionType.SUCCESS, False, datetime.now(), ExerciseType.PROBLEM),
            (InteractionType.FAILURE, False, datetime.now(), ExerciseType.PROBLEM),
            (InteractionType.ATTEMPT, True, datetime.now(), ExerciseType.PROBLEM),
            (InteractionType.SUCCESS, False, datetime.now(), ExerciseType.PROBLEM),
            (InteractionType.SUCCESS, False, datetime.now(), ExerciseType.PROBLEM),
        ])
        actual = Planner._mastery_eligible(exercise)
        self.assertTrue(actual)

    def test_fail_eligible(self):
        exercise = make_exercise([(InteractionType.ATTEMPT, False, datetime.now(), ExerciseType.PROBLEM),
                                  (InteractionType.SUCCESS, False, datetime.now(), ExerciseType.PROBLEM),
                                  (InteractionType.FAILURE, False, datetime.now(), ExerciseType.PROBLEM),
                                  (InteractionType.FAILURE, False, datetime.now(), ExerciseType.PROBLEM)])
        actual = Planner._fail_eligible(exercise)
        self.assertTrue(actual)

        exercise = make_exercise([(InteractionType.ATTEMPT, False, datetime.now(), ExerciseType.PROBLEM),
                                  (InteractionType.SUCCESS, False, datetime.now(), ExerciseType.PROBLEM),
                                  (InteractionType.ATTEMPT, False, datetime.now(), ExerciseType.PROBLEM),
                                  (InteractionType.ATTEMPT, True, datetime.now(), ExerciseType.PROBLEM)])
        actual = Planner._fail_eligible(exercise)
        self.assertTrue(actual)

        exercise = make_exercise([(InteractionType.ATTEMPT, False, datetime.now(), ExerciseType.PROBLEM)])
        actual = Planner._fail_eligible(exercise)
        self.assertFalse(actual)

        exercise = make_exercise([(InteractionType.ATTEMPT, True, datetime.now(), ExerciseType.PROBLEM)])
        actual = Planner._fail_eligible(exercise)
        self.assertTrue(actual)

        exercise = make_exercise([(InteractionType.FAILURE, False, datetime.now(), ExerciseType.PROBLEM),
                                  (InteractionType.SUCCESS, False, datetime.now(), ExerciseType.PROBLEM)])
        actual = Planner._fail_eligible(exercise)
        self.assertFalse(actual)

        exercise = make_exercise([(InteractionType.FAILURE, False, datetime.now(), ExerciseType.PROBLEM),
                                  (InteractionType.ATTEMPT, False, datetime.now(), ExerciseType.PROBLEM)])
        actual = Planner._fail_eligible(exercise)
        self.assertFalse(actual)

    def test_new_eligible(self):
        exercise = make_exercise([(InteractionType.ATTEMPT, False, datetime.now(), ExerciseType.PROBLEM)])
        actual = Planner._new_eligible(exercise)
        self.assertTrue(actual)

        exercise = make_exercise([(InteractionType.ATTEMPT, True, datetime.now(), ExerciseType.PROBLEM)])
        actual = Planner._new_eligible(exercise)
        self.assertFalse(actual)

        exercise = make_exercise([(InteractionType.FAILURE, False, datetime.now(), ExerciseType.PROBLEM),
                                  (InteractionType.ATTEMPT, False, datetime.now(), ExerciseType.PROBLEM)])
        actual = Planner._new_eligible(exercise)
        self.assertFalse(actual)

