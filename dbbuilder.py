import sqlite3

# This is a script for building the database from scratch.
query = open('database_builder.sql', 'r').read()
db = sqlite3.connect('mathdb.sqlite')
c = db.cursor()
c.executescript(query)
db.commit()
db.close()

