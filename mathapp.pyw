import sys

from main_menu import MainMenu
from textbook_editor import TextbookEditor
from textbook_menu import TextbookMenu
from exercise_solver import Solver
from trainer import *
from dialogs import *

__version__ = "0.7.0"


class MainWindow(QMainWindow):
    main_menu_opened = pyqtSignal()
    textbook_opened = pyqtSignal(int)
    new_textbook_made = pyqtSignal(dict)
    new_chapter_made = pyqtSignal(dict)
    new_section_made = pyqtSignal(dict)

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self._central_widget = QStackedWidget()
        self._main_menu = MainMenu(self)
        self._textbook_menu = TextbookMenu(self)
        self._textbook_editor = TextbookEditor(self)
        self._trainer = Trainer()
        self._solver = Solver(self)
        self._central_widget.addWidget(self._main_menu)
        self._central_widget.addWidget(self._textbook_menu)
        self._central_widget.addWidget(self._textbook_editor)
        self._central_widget.addWidget(self._solver)
        self.setCentralWidget(self._central_widget)

        # Main Menu Connections:
        self.new_textbook_made.connect(self._trainer.add_textbook)
        self._trainer.display_bookshelf.connect(self._main_menu.bookshelf.add_books)
        self._trainer.display_bookshelf.connect(self._connect_main_menu_bookshelf)
        self.main_menu_opened.connect(self._trainer.update_bookshelf)
        self._main_menu.menu.add_textbook_button.clicked.connect(self._add_textbook_button_handler)
        self._main_menu.menu.study_math_foundations.clicked.connect(
            lambda: self._trainer.prepare_study_group('math_foundations'))
        self._main_menu.menu.study_math_foundations.clicked.connect(lambda: self._central_widget.setCurrentIndex(3))

        # Text Menu Connections:
        self._textbook_menu.main_menu_button_clicked.connect(lambda: self._central_widget.setCurrentIndex(0))
        self._textbook_menu.main_menu_button_clicked.connect(self.main_menu_opened.emit)
        self._textbook_menu.left_menu.add_chapter_button.clicked.connect(self._add_chapter_button_handler)
        self._textbook_menu.left_menu.add_section_button.clicked.connect(self._add_section_button_handler)
        self._textbook_menu.right_menu.add_content_button.clicked.connect(self._trainer.add_content_button_clicked)
        self._textbook_menu.right_menu.add_content_button.clicked.connect(lambda: self._central_widget.setCurrentIndex(2))
        self._textbook_menu.study_desired.connect(lambda: self._central_widget.setCurrentIndex(3))
        self._textbook_menu.study_desired.connect(self._trainer.prepare_study)
        self._trainer.display_textbook_contents.connect(lambda names, current: self._textbook_menu.update_layout(names, current))
        self._trainer.display_textbook_contents.connect(self._connect_chapter_buttons)

        # Textbook Editor Connections:
        self._textbook_editor.menu.return_to_textbook_button.clicked.connect(lambda: self._central_widget.setCurrentIndex(1))
        self._textbook_editor.exercise_chosen.connect(self._exercise_added_handler)
        self._trainer.display_chapter_problems.connect(self._add_exercise_buttons)
        self._trainer.display_exercise_content.connect(self._textbook_editor.display_exercise)
        self._textbook_editor.exercise_delete.connect(self._trainer.delete_exercise)
        self._textbook_editor.exercise_activate.connect(self._trainer.activate_exercise)

        # Solver Connections:
        self._solver.return_to_textbook.connect(lambda: self._central_widget.setCurrentIndex(1))
        self._solver.return_to_textbook.connect(lambda: self._trainer.chapter_button_clicked(0))
        self._trainer.display_problem.connect(self._solver.display_problem)
        self._solver.show_solution.connect(self._trainer.show_solution_clicked)
        self._trainer.display_solution.connect(self._solver.display_solution)
        self._solver.success_button_clicked.connect(self._trainer.success_button_clicked)
        self._solver.failure_button_clicked.connect(self._trainer.failure_button_clicked)
        # Todo I would like to have a popup in addition to returning to the main menu when a study session is over.
        self._trainer.study_session_over.connect(lambda: self._central_widget.setCurrentIndex(0))

        # General Connections:
        self.textbook_opened.connect(self._trainer.open_textbook)
        self.new_chapter_made.connect(self._trainer.add_chapter)
        self.new_section_made.connect(self._trainer.add_section)

        self.main_menu_opened.emit()

    def _exercise_added_handler(self, exercise_type, data):
        self._trainer.add_exercise(exercise_type, data)

    def _connect_main_menu_bookshelf(self):
        for book in self._main_menu.bookshelf.textbooks:
            book.textbook_clicked.connect(lambda book_id: self.textbook_opened.emit(book_id))
            book.textbook_clicked.connect(lambda: self._central_widget.setCurrentIndex(1))

    def _connect_chapter_buttons(self):
        for chapter_button in self._textbook_menu.textbook_content_menu.chapter_buttons:
            chapter_button.chapter_button_clicked.connect(self._trainer.chapter_button_clicked)

            section_buttons = self._textbook_menu.textbook_content_menu.chapter_buttons[chapter_button]

            if section_buttons is not None:
                self._connect_section_buttons(section_buttons)

    def _connect_section_buttons(self, section_buttons):
        for section_button in section_buttons:
            section_button.clicked.connect(self._trainer.section_button_clicked)

    def _add_exercise_buttons(self, exercises):
        if len(self._textbook_editor.exercise_buttons) != 0:
            for button in self._textbook_editor.exercise_buttons:
                button.exercise_button_clicked.disconnect(self._exercise_content_desired)
            self._textbook_editor.exercise_buttons.clear()

        self._textbook_editor.add_exercises(exercises)

        for button in self._textbook_editor.exercise_buttons:
            button.exercise_button_clicked.connect(self._exercise_content_desired)

    def _exercise_content_desired(self, exercise_id):
        self._trainer.exercise_button_clicked(exercise_id)

    def _add_section_button_handler(self):
        dialog = NewChapterDialog(self)
        if dialog.exec_():
            new_section_values = dialog.get_values(dialog)
            self.new_section_made.emit(new_section_values)

    def _add_chapter_button_handler(self):
        dialog = NewChapterDialog(self)
        if dialog.exec_():
            new_chapter_values = dialog.get_values(dialog)
            self.new_chapter_made.emit(new_chapter_values)

    def _add_textbook_button_handler(self):
        dialog = NewTextbookDialog(self)
        if dialog.exec_():
            textbook_values = dialog.get_values_textbook_dialog(dialog)
            self.new_textbook_made.emit(textbook_values)


def main():
    app = QApplication(sys.argv)
    view = MainWindow()
    view.show()
    app.exec_()


main()
