from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

from exercisetype import ExerciseType
from ast import literal_eval
import uuid


class ProblemHolder(QWidget):
    add_exercise_clicked = pyqtSignal(ExerciseType)
    delete_exercise_clicked = pyqtSignal()
    activated_checkbox_clicked = pyqtSignal(bool)

    def __init__(self, parent, _exercise_type):
        super(ProblemHolder, self).__init__(parent)

        if _exercise_type not in (ExerciseType.PROOF, ExerciseType.EXAMPLE, ExerciseType.PROBLEM):
            raise ValueError(f'{_exercise_type} not a supported type')

        self.type = _exercise_type

        # Widgets:
        if self.type in (ExerciseType.PROBLEM, ExerciseType.EXAMPLE):
            front_text = 'Problem'
            back_text = 'Solution'
        elif self.type is ExerciseType.PROOF:
            front_text = 'Statement'
            back_text = 'Proof'
        else:
            raise ValueError(f'{_exercise_type} is not supported.')

        self._instruction_input = ExerciseTextEdit(self, 'Instruction')
        self._instruction_input.hide()
        self._note_input = ExerciseTextEdit(self, 'Note')
        self._note_input.hide()
        self._problem_input = ExerciseTextEdit(self, front_text)
        self._solution_input = ExerciseTextEdit(self, back_text)

        solution_2_button = QPushButton('Solution 2')
        solution_3_button = QPushButton('Solution 3')
        solution_4_button = QPushButton('solution 4')
        solution_2_button.setDisabled(True)
        solution_3_button.setDisabled(True)
        solution_4_button.setDisabled(True)

        self._is_activated_checkbox = QCheckBox('Activated')
        self._is_activated_checkbox.setChecked(True)
        self._is_homework_checkbox = QCheckBox('Homework')
        self._is_difficult_checkbox = QCheckBox('Difficult')

        self.add_problem_button = QPushButton(f'Add {_exercise_type.name.lower()}')
        self.delete_problem_button = QPushButton(f'Delete {_exercise_type.name.lower()}')
        self.delete_problem_button.setDisabled(True)
        self.add_problem_button.clicked.connect(lambda: self.add_exercise_clicked.emit(_exercise_type))
        self._is_activated_checkbox.clicked.connect(lambda x: self.activated_checkbox_clicked.emit(x))
        self.delete_problem_button.clicked.connect(lambda: self.delete_exercise_clicked.emit())

        # Layout:
        checkbox_holder = QWidget()
        checkbox_holder_layout = QHBoxLayout()
        checkbox_holder_layout.addWidget(self._is_activated_checkbox)
        checkbox_holder_layout.addWidget(self._is_homework_checkbox)
        checkbox_holder_layout.addWidget(self._is_difficult_checkbox)
        checkbox_holder.setLayout(checkbox_holder_layout)

        add_delete_button_holder = QWidget()
        layout_add_delete_button_holder = QHBoxLayout()
        layout_add_delete_button_holder.addWidget(self.delete_problem_button)
        layout_add_delete_button_holder.addWidget(self.add_problem_button)
        add_delete_button_holder.setLayout(layout_add_delete_button_holder)

        layout = QGridLayout()
        layout.addWidget(self._instruction_input, 0, 0)
        layout.addWidget(self._problem_input, 1, 0)
        layout.addWidget(self._solution_input, 3, 0)
        layout.addWidget(checkbox_holder, 4, 0)
        layout.addWidget(add_delete_button_holder, 4, 1)
        self.setLayout(layout)

    def clear_data(self):
        self._instruction_input.input.clear()
        self._note_input.input.clear()
        self._problem_input.input.clear()
        self._solution_input.input.clear()
        self.delete_problem_button.setDisabled(True)

    def get_data(self):
        instruction_data = self._instruction_input.get_data()
        note_data = self._note_input.get_data()
        problem_data = self._problem_input.get_data()
        solution_data = self._solution_input.get_data()
        is_activated = self._is_activated_checkbox.isChecked()

        return {'instruction': instruction_data, 'note': note_data, 'problem': problem_data,
                'solution': solution_data, 'is_difficult': self._is_difficult_checkbox.checkState(),
                'is_homework': self._is_homework_checkbox.checkState(), 'activated': is_activated}

    def update_content(self, problem_content):
        self._problem_input.put_content(problem_content['problem'])
        self._solution_input.put_content(problem_content['solution'])
        self.delete_problem_button.setDisabled(False)


class FactHolder(QWidget):
    add_exercise_clicked = pyqtSignal(ExerciseType)
    delete_exercise_clicked = pyqtSignal()
    activated_checkbox_clicked = pyqtSignal(bool)

    def __init__(self, parent, _exercise_type):
        super(FactHolder, self).__init__(parent)

        if _exercise_type not in (ExerciseType.FACT, ExerciseType.DEFINITION, ExerciseType.THEOREM):
            raise ValueError(f'{_exercise_type} not a supported type')

        self.type = _exercise_type

        # Widgets:
        if self.type is ExerciseType.DEFINITION:
            front_text = 'Definiendum'
            back_text = 'Definines'
        elif self.type is ExerciseType.THEOREM:
            front_text = 'Description'
            back_text = 'Theorem'
        elif self.type is ExerciseType.FACT:
            front_text = 'Question'
            back_text = 'Answer'
        else:
            raise ValueError(f'{self.type} is not supported.')

        self._front = ExerciseTextEdit(self, front_text)
        self._back = ExerciseTextEdit(self, back_text)
        self._note_input = ExerciseTextEdit(self, 'Note')
        self._note_input.hide()

        self._is_activated_checkbox = QCheckBox('Activated')
        self._is_activated_checkbox.setChecked(True)

        self.add_exercise_button = QPushButton(f'Add {_exercise_type.name.lower()}')
        self.delete_exercise_button = QPushButton(f'Delete {_exercise_type.name.lower()}')
        self.delete_exercise_button.setDisabled(True)
        self.add_exercise_button.clicked.connect(lambda: self.add_exercise_clicked.emit(_exercise_type))
        self.delete_exercise_button.clicked.connect(lambda: self.delete_exercise_clicked.emit())
        self._is_activated_checkbox.clicked.connect(lambda x: self.activated_checkbox_clicked.emit(x))

        add_delete_button_holder = QWidget()
        layout_add_delete_button_holder = QHBoxLayout()
        layout_add_delete_button_holder.addWidget(self.delete_exercise_button)
        layout_add_delete_button_holder.addWidget(self.add_exercise_button)
        add_delete_button_holder.setLayout(layout_add_delete_button_holder)

        checkbox_holder = QWidget()
        checkbox_holder_layout = QHBoxLayout()
        checkbox_holder_layout.addWidget(self._is_activated_checkbox)
        checkbox_holder.setLayout(checkbox_holder_layout)

        layout = QGridLayout()
        layout.addWidget(self._front, 0, 0)
        layout.addWidget(self._back, 1, 0)
        layout.addWidget(add_delete_button_holder, 4, 1)
        layout.addWidget(checkbox_holder, 4, 0)
        self.setLayout(layout)

    def clear_data(self):
        self._front.input.clear()
        self._back.input.clear()
        self._note_input.input.clear()

        self.delete_exercise_button.setDisabled(True)

    def get_data(self):
        front_data = self._front.get_data()
        back_data = self._back.get_data()
        note_data = self._note_input.get_data()
        is_activated = self._is_activated_checkbox.isChecked()
        return {'front': front_data, 'note': note_data, 'back': back_data,  'activated': is_activated}

    def update_content(self, problem_content):
        self._front.put_content(problem_content['problem'])
        self._back.put_content(problem_content['solution'])
        self.delete_exercise_button.setDisabled(False)


class TopicHolder(QWidget):
    add_exercise_clicked = pyqtSignal(ExerciseType)
    delete_exercise_clicked = pyqtSignal()
    activated_checkbox_clicked = pyqtSignal(bool)

    def __init__(self, parent, _exercise_type):
        super(TopicHolder, self).__init__(parent)

        if _exercise_type is not ExerciseType.LECTURE_TOPIC:
            raise ValueError(f'{_exercise_type} not a supported type')

        self.type = _exercise_type

        # Widgets:
        self._topic = ExerciseTextEdit(self, 'Topic')
        self._note_input = ExerciseTextEdit(self, 'Note')
        self._note_input.hide()
        self._is_activated_checkbox = QCheckBox('Activated')
        self._is_activated_checkbox.setChecked(True)

        self.add_exercise_button = QPushButton(f'Add Lecture Topic')
        self.delete_exercise_button = QPushButton(f'Delete Lecture Topic')
        self.delete_exercise_button.setDisabled(True)
        self.add_exercise_button.clicked.connect(lambda: self.add_exercise_clicked.emit(_exercise_type))
        self.delete_exercise_button.clicked.connect(lambda: self.delete_exercise_clicked.emit())
        self._is_activated_checkbox.clicked.connect(lambda x: self.activated_checkbox_clicked.emit(x))

        add_delete_button_holder = QWidget()
        layout_add_delete_button_holder = QHBoxLayout()
        layout_add_delete_button_holder.addWidget(self.delete_exercise_button)
        layout_add_delete_button_holder.addWidget(self.add_exercise_button)
        add_delete_button_holder.setLayout(layout_add_delete_button_holder)

        checkbox_holder = QWidget()
        checkbox_holder_layout = QHBoxLayout()
        checkbox_holder_layout.addWidget(self._is_activated_checkbox)
        checkbox_holder.setLayout(checkbox_holder_layout)

        layout = QGridLayout()
        layout.addWidget(self._topic, 0, 0)
        layout.addWidget(add_delete_button_holder, 4, 1)
        layout.addWidget(checkbox_holder, 4, 0)
        self.setLayout(layout)

    def clear_data(self):
        self._topic.input.clear()
        self._note_input.input.clear()

        self.delete_exercise_button.setDisabled(True)

    def get_data(self):
        topic_data = self._topic.get_data()
        note_data = self._note_input.get_data()
        is_activated = self._is_activated_checkbox.isChecked()
        return {'topic': topic_data, 'note': note_data, 'activated': is_activated}

    def update_content(self, problem_content):
        self._topic.put_content(problem_content['problem'])
        self.delete_exercise_button.setDisabled(False)


class ExerciseTextEdit(QWidget):
    def __init__(self, parent, text):
        super(ExerciseTextEdit, self).__init__(parent)

        self._data = {}

        label = QLabel(text)
        self.input = self.QTextEditWrapper(self)
        label.setBuddy(self.input)

        layout = QVBoxLayout()
        layout.addWidget(label)
        layout.addWidget(self.input)

        self.setLayout(layout)

    def put_content(self, content):
        self.input.clear()


    def get_data(self):
        self._data['html'] = self.input.toHtml()
        return self._data

    class QTextEditWrapper(QTextEdit):
        def __init__(self, parent):
            super(ExerciseTextEdit.QTextEditWrapper, self).__init__(parent)
            self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
            self.parent = parent

        def mouseReleaseEvent(self, event: QMouseEvent):
            if event.button() == Qt.MiddleButton:
                event.ignore()
            else:
                event.accept()

        def keyPressEvent(self, e: QKeyEvent):

            if e.key() == Qt.Key_V and e.modifiers() == Qt.ControlModifier:
                clipboard = QApplication.clipboard()
                if clipboard.mimeData().hasImage():
                    image = clipboard.mimeData().imageData()
                    pixmap = QPixmap(image)
                    image_name = self.add_image_to_collection(pixmap)
                    ratio = pixmap.width()/pixmap.height()
                    new_height = 470/ratio

                    cursor = self.textCursor()
                    cursor.insertImage('images/' + image_name)
            else:
                QTextEdit.keyPressEvent(self, e)

        @staticmethod
        def add_image_to_collection(pixmap):
            # preferable it would be something that has to do with the actual textbook, section, type of exercise, etc.
            image_id = str(uuid.uuid4()) + '.jpg'
            pixmap.save('images/' + image_id, 'JPG', 100)
            return image_id

        def canInsertFromMimeData(self, source):
            if source.hasImage():
                return True
            else:
                return QTextEdit.canInsertFromMimeData(self, source)


