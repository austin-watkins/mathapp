import sqlite3
import collections

from exercisetype import ExerciseType
from interaction_type import InteractionType


class Section:
    @staticmethod
    def insert_new_section(section_info, current_textbook, current_chapter):
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        max_section_number = c.execute('SELECT MAX(SectionID) '
                                       'FROM SECTION WHERE TextbookID=? and ChapterID=?',
                                       (str(current_textbook), str(current_chapter))).fetchone()[0]
        new_section_number = max_section_number + 1 if max_section_number is not None else 0

        c.execute('INSERT INTO SECTION VALUES (?,?,?,?,?,?,?)', (str(current_textbook), str(current_chapter),
                                                                 str(new_section_number), section_info['name'],
                                                                 section_info['number_of_problems'], None, 0))
        db.commit()
        db.close()

    @staticmethod
    def get_sections(textbook_id, chapter_table):
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()

        chapter_index = 0
        keys = list(chapter_table.keys())  # Must make a copy because of mutation during iteration.
        for chapter_name in keys:

            chapter_sections = []
            for section_tuple in c.execute('SELECT Name FROM SECTION WHERE TextbookID=? AND ChapterID=?',
                                           (str(textbook_id), chapter_index)):
                chapter_sections.append(section_tuple[0])

            chapter_table.update()
            chapter_table[chapter_name] = chapter_sections
            chapter_index = chapter_index + 1

        db.close()
        return chapter_table

    @staticmethod
    def get_section_id(textbook_id, chapter_id, button_text):
        print(button_text)
        # todo I should make it so that two sections with the same name can't exist for the same chapter
        # note this could mean that the other method is better (having buttons know their index). Who knows.
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        section_index = c.execute('SELECT SectionID'
                                  ' FROM SECTION '
                                  'where TextbookID=? AND ChapterID=? AND Name=?',
                                  (str(textbook_id), str(chapter_id), button_text)).fetchone()[0]
        db.close()
        return section_index


class Chapter:
    @staticmethod
    def get_new_chapter_id(current_textbook):
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        max_chapter_id = c.execute('SELECT MAX(ChapterID) '
                                   'FROM CHAPTER '
                                   'WHERE TextbookID=?',
                                   (str(current_textbook),)).fetchone()[0]
        new_chapter_id = max_chapter_id + 1 if max_chapter_id is not None else 0
        db.close()
        return new_chapter_id

    @staticmethod
    def insert_new_chapter(chapter_info, current_textbook):
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        new_chapter_id = Chapter.get_new_chapter_id(current_textbook)

        c.execute('INSERT INTO CHAPTER VALUES (?,?,?,?,?)', (str(current_textbook), str(new_chapter_id),
                                                             chapter_info['name'], chapter_info['number_of_problems'],
                                                             None))
        db.commit()
        db.close()

    @staticmethod
    def get_chapters(textbook_id):
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()

        chapter_names = collections.OrderedDict()
        query_results = c.execute('SELECT Name FROM CHAPTER WHERE TextbookID=?', (str(textbook_id),)).fetchall()

        for chapter_index in range(len(query_results)):
            name = query_results[chapter_index][0]
            chapter_names[name] = None

        db.close()
        return chapter_names

    @staticmethod
    def get_chapter_name(textbook_id, chapter_id):
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        chapter_name = c.execute('SELECT Name FROM CHAPTER WHERE TextbookID=? AND ChapterID=?',
                                 (str(textbook_id), str(chapter_id))).fetchone()[0]
        db.close()
        return chapter_name

    @staticmethod
    def insert_new_interaction(textbook_id, chapter_id, section_id, exercise_id, interaction):
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()

        ending = interaction['ending'].value
        duration = interaction['duration']
        date = interaction['date']

        identification_values = (textbook_id, chapter_id, section_id, exercise_id)

        max_interaction_number = \
            c.execute('SELECT MAX(InteractionNumber) FROM INTERACTION WHERE TextbookID=? and ChapterID=? and '
                      'SectionID=? and ExerciseID=?', identification_values).fetchone()[0]
        print(f"max interaction number is {max_interaction_number}")
        interaction_number = 0 if max_interaction_number is None else max_interaction_number + 1
        print(f'interaction: {interaction_number}, ending={InteractionType(ending)}')

        interaction_content = [(textbook_id, chapter_id, section_id, exercise_id, interaction_number, ending, duration, date)]
        c.executemany('INSERT INTO INTERACTION VALUES (?,?,?,?,?,?,?,?)', interaction_content)
        db.commit()

        db.close()


class Textbook:
    @staticmethod
    def record_new_textbook(textbook_settings):
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        max_textbook_id = c.execute('SELECT MAX(TextbookID) FROM TEXTBOOK').fetchone()[0]
        new_textbook_id = max_textbook_id + 1 if max_textbook_id is not None else 0
        Textbook.insert_new_textbook(c, textbook_settings['textbook_values'], new_textbook_id)
        Textbook.insert_new_exercise_settings(c, textbook_settings['exercise_settings_values'], new_textbook_id)
        Textbook.insert_new_mastery_schedule(c, textbook_settings['mastery_schedule_values'], new_textbook_id)
        Textbook.insert_new_textbook_topics(c, textbook_settings['textbook_values'], new_textbook_id)
        db.commit()
        db.close()

    @staticmethod
    def insert_new_textbook(c, textbook_settings, number_of_textbooks):
        # Insert Into Textbook
        textbook_record = (number_of_textbooks, 'NA', textbook_settings['ForcedAttempts'],
                           textbook_settings['Name'], None, None, textbook_settings['ProblemsPerDay'],
                           textbook_settings['DaysToReadSection'],
                           1 if textbook_settings['HasSectionProblems'] else 0,
                           1 if textbook_settings['HasChapterProblems'] else 0)
        c.execute('INSERT INTO TEXTBOOK VALUES (?,?,?,?,?,?,?,?,?,?)', textbook_record)

    @staticmethod
    def insert_new_textbook_topics(c, textbook_settings, number_of_textbooks):
        topics = textbook_settings['TextbookTopics'].split(',')
        for topic in topics:
            if topic != '':
                c.execute('INSERT INTO TEXTBOOK_TOPIC VALUES (?,?)', (number_of_textbooks, topic.strip()))

    @staticmethod
    def insert_new_mastery_schedule(c, mastery_schedule, number_of_textbooks):
        schedule_duration_exercise = mastery_schedule['schedule_duration_exercise']
        for i in range(len(schedule_duration_exercise)):
            mastery_schedule_exercises = [(number_of_textbooks, 'problem', i, schedule_duration_exercise[i]),
                                          (number_of_textbooks, 'fact', i, schedule_duration_exercise[i]),
                                          (number_of_textbooks, 'proof', i, schedule_duration_exercise[i]),
                                          (number_of_textbooks, 'lecture', i, schedule_duration_exercise[i])]
            c.executemany('INSERT INTO MASTERY_SCHEDULE VALUES (?,?,?,?)', mastery_schedule_exercises)
        schedule_duration_core = mastery_schedule['schedule_duration_core']
        for i in range(len(schedule_duration_core)):
            mastery_schedule_core = [(number_of_textbooks, 'definition', i, schedule_duration_core[i]),
                                     (number_of_textbooks, 'theorem', i, schedule_duration_core[i])]
            c.executemany('INSERT INTO MASTERY_SCHEDULE VALUES (?,?,?,?)', mastery_schedule_core)

    @staticmethod
    def insert_new_exercise_settings(c, exercise_settings, number_of_textbooks):
        problem_review_counter = exercise_settings['problem_review_counter']
        problem_failure_counter = exercise_settings['problem_failure_counter']
        core_review_counter = exercise_settings['core_review_counter']
        core_failure_counter = exercise_settings['core_failure_counter']

        exercise_settings = [(number_of_textbooks, 'problem', problem_review_counter, problem_failure_counter),
                             (number_of_textbooks, 'fact', problem_review_counter, problem_failure_counter),
                             (number_of_textbooks, 'proof', problem_review_counter, problem_failure_counter),
                             (number_of_textbooks, 'lecture', problem_review_counter, problem_failure_counter),
                             (number_of_textbooks, 'definition', core_review_counter, core_failure_counter),
                             (number_of_textbooks, 'theorem', core_review_counter, core_failure_counter)]
        c.executemany('INSERT INTO EXERCISE_SETTINGS VALUES (?,?,?,?)', exercise_settings)

    @staticmethod
    def get_exercise_settings(textbook_id):
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        exercise_settings = c.execute(
            'SELECT Type, ReviewCounter, FailureCounter FROM EXERCISE_SETTINGS WHERE TextbookID=?',
            (textbook_id,)).fetchall()
        db.close()
        return exercise_settings

    @staticmethod
    def get_mastery_schedule(textbook_id):
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        mastery_schedule = c.execute('SELECT Type, Step, Duration FROM MASTERY_SCHEDULE WHERE TextbookID=?',
                                     (textbook_id,)).fetchall()
        db.close()
        return mastery_schedule


class Exercise:
    @staticmethod
    def insert_problem(textbook_id, chapter_id, section_id, exercise_type, data):
        print('adding problem to (', textbook_id, ', ', chapter_id, ', ', section_id, ')')
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        identification_values = (str(textbook_id), str(chapter_id), str(section_id))

        exercise_number = Exercise.get_new_exercise_id(identification_values)
        print(exercise_number)

        exercise_values = (
            str(textbook_id), str(chapter_id), str(section_id), str(exercise_number), str(exercise_type.value),
            str(data['note']), data['activated'])
        c.execute('INSERT INTO EXERCISE VALUES (?,?,?,?,?,?,?)', exercise_values)

        problem_values = (str(textbook_id), str(chapter_id), str(section_id), str(exercise_number),
                          str(data['problem']), str(data['instruction']), str(data['is_difficult']),
                          str(data['solution']), str(data['is_homework']))

        c.execute('INSERT INTO PROBLEM VALUES (?,?,?,?,?,?,?,?,?)', problem_values)

        db.commit()
        db.close()

    @staticmethod
    def insert_definition(textbook_id, chapter_id, section_id, exercise_type, data):
        print('adding problem to (', textbook_id, ', ', chapter_id, ', ', section_id, ')')
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        identification_values = (str(textbook_id), str(chapter_id), str(section_id))

        exercise_number = Exercise.get_new_exercise_id(identification_values)
        print(exercise_number)

        exercise_values = (
            str(textbook_id), str(chapter_id), str(section_id), str(exercise_number), str(exercise_type.value),
            str(data['note']), data['activated'])
        c.execute('INSERT INTO EXERCISE VALUES (?,?,?,?,?,?,?)', exercise_values)

        definition_values = (str(textbook_id), str(chapter_id), str(section_id), str(exercise_number),
                             str(data['front']), str(data['back']))
        c.execute('INSERT INTO FACT VALUES (?,?,?,?,?,?,?)', definition_values)

        db.commit()
        db.close()

    @staticmethod
    def insert_topic(textbook_id, chapter_id, section_id, exercise_type, data):
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        identification_values = (str(textbook_id), str(chapter_id), str(section_id))

        exercise_number = Exercise.get_new_exercise_id(identification_values)

        exercise_values = (
            str(textbook_id), str(chapter_id), str(section_id), str(exercise_number), str(exercise_type.value),
            str(data['note']), True)
        c.execute('INSERT INTO EXERCISE VALUES (?,?,?,?,?,?,?)', exercise_values)

        topic_values = (str(textbook_id), str(chapter_id), str(section_id), str(exercise_number),
                        str(data['topic']))
        c.execute('INSERT INTO LECTURE_TOPIC VALUES (?,?,?,?,?)', topic_values)

        db.commit()
        db.close()

    @staticmethod
    def get_new_exercise_id(identification_values):
        """
        Gets the max plus one exercise id from the database. If there is none returns one.
        :param identification_values: tuple (TextbookID, ChapterID, and SectionID)
        :return: unique integral exercise number
        """
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        max_exercise_number = \
            c.execute('SELECT MAX(ExerciseID) FROM EXERCISE WHERE TextbookID=? and ChapterID=? and SectionID=?',
                      identification_values).fetchone()[0]
        db.commit()
        db.close()
        new_exercise_number = max_exercise_number + 1 if max_exercise_number is not None else 0
        return new_exercise_number

    @staticmethod
    def set_activated(textbook_id, chapter_id, section_id, exercise_id, activated: bool) -> None:
        """
        Given an exercise set the IsActivated parameter in the database for that exercise.
        """

        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        values = (activated, textbook_id, chapter_id, section_id if section_id is not None else 'None', exercise_id)
        c.execute(
            'UPDATE EXERCISE '
            'SET IsActivated = ? '
            'WHERE TextbookID=? AND ChapterID=? AND SectionID=? AND ExerciseID=?',
            values)
        db.commit()
        db.close()

    @staticmethod
    def exercise_exists(textbook_id, chapter_id, section_id, exercise_id) -> bool:
        """
        Given the textbook, chapter, section, and exercise returns if that exercise actually exists.

        If it doesn't exist then it probably is in the process of being created.

        :return: True if the problem exists in the database otherwise False
        """
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        values = (textbook_id, chapter_id, section_id if section_id is not None else 'None', exercise_id)

        exercise = c.execute(
            'SELECT ExerciseId FROM EXERCISE WHERE TextbookID=? AND ChapterID=? AND SectionID=? AND ExerciseID=?',
            values).fetchall()
        db.close()
        return len(exercise) > 0

    @staticmethod
    def delete_exercise(textbook_id, chapter_id, section_id, exercise_id):
        current_exercise_type = Exercise.get_exercise_type(textbook_id, chapter_id, section_id, exercise_id)
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        values = (textbook_id, chapter_id, section_id if section_id is not None else 'None', exercise_id)
        print(f'Deleting: {values}')

        if current_exercise_type in (ExerciseType.EXAMPLE, ExerciseType.PROBLEM, ExerciseType.PROOF):
            table = 'PROBLEM'
        elif current_exercise_type in (ExerciseType.DEFINITION, ExerciseType.THEOREM, ExerciseType.FACT):
            table = 'FACT'
        elif current_exercise_type is ExerciseType.LECTURE_TOPIC:
            table = 'LECTURE_TOPIC'

        else:
            raise NotImplementedError('Ability to delete item other than exercise Not supported')

        c.execute(f'DELETE FROM {table} WHERE TextbookID=? AND ChapterID=? AND SectionID=?'
                  ' AND ExerciseID=?', values)

        c.execute('DELETE FROM EXERCISE WHERE TextbookID=? AND ChapterID=? AND SectionID=?'
                  ' AND ExerciseID=?', values)
        c.execute('DELETE FROM INTERACTION WHERE TextbookID=? AND ChapterID=? AND SectionID=?'
                  ' AND ExerciseID=?', values)

        db.commit()
        db.close()

    # fixme I don't like how there is so much code duplication between this function and get_textbook_exercises
    @staticmethod
    def get_section_exercises(textbook_id, chapter_id, section_id, activated=True):
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        # todo I don't think that I need note, and I can get it when I get content.
        if activated:
            values = (textbook_id, chapter_id, section_id if section_id is not None else 'None', activated)
            exercises = c.execute('SELECT TextbookID, ChapterID, SectionID, ExerciseID, Type FROM EXERCISE '
                                  'WHERE TextbookID=? AND ChapterID=? AND SectionID=? AND IsActivated=?',
                                  values).fetchall()
        else:
            values = (textbook_id, chapter_id, section_id if section_id is not None else 'None')
            exercises = c.execute('SELECT TextbookID, ChapterID, SectionID, ExerciseID, Type FROM EXERCISE '
                                  'WHERE TextbookID=? AND ChapterID=? AND SectionID=?', values).fetchall()

        db.close()
        return exercises

    @staticmethod
    def get_textbook_exercises(textbook_id):
        """
        Get all activated exercies from the database for a particular textbook.

        :param textbook_id:  the textbook whose activated exercises we want.
        :return:
        """
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        exercises = c.execute(
            'SELECT TextbookID, ChapterID, SectionID, ExerciseID, Type FROM EXERCISE WHERE TextbookID=? AND IsActivated=?',
            (textbook_id, True)).fetchall()
        db.close()
        return exercises

    @staticmethod
    def get_textbook_exercise_interactions(textbook_id, particular_section=None):
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        if particular_section is None:
            interactions = c.execute(
                'SELECT TextbookID, ChapterID, SectionID, ExerciseID, InteractionNumber, Ending, Duration, Date FROM INTERACTION WHERE TextbookID=?',
                (textbook_id,)).fetchall()
        else:
            chapter_id = particular_section['chapter']
            section_id = particular_section['section']
            values = (textbook_id, chapter_id, section_id if section_id is not None else 'None')
            interactions = c.execute(
                'SELECT TextbookID, ChapterID, SectionID, ExerciseID, InteractionNumber, Ending, Duration,'
                ' Date FROM INTERACTION'
                ' WHERE TextbookID=? AND ChapterID=? AND SectionID=?', values).fetchall()

        db.close()
        return interactions

    @staticmethod
    def get_content(textbook_id, chapter_id, section_id, exercise_id):
        """
        Queries the database to get the hint, problem, instruction, and solution for a given problem.
        :param textbook_id: The textbook where the exercise is.
        :param chapter_id: The chapter where the exercise is.
        :param section_id: The section where the exercise is.
        :param exercise_id: The exercise number within that section or chapter.
        :return: (hint, problem, instruction, solution)
        """
        current_exercise_type = Exercise.get_exercise_type(textbook_id, chapter_id, section_id, exercise_id)
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        values = (textbook_id, chapter_id, section_id if section_id is not None else 'None', exercise_id)

        # Due to the keys being generic for all exercise types. It is important that you query the database for items
        # in this order. Currently instructions are not supported, but I would like to concatenate them to the front.
        if current_exercise_type in (ExerciseType.PROBLEM, ExerciseType.EXAMPLE, ExerciseType.PROOF):
            exercise_content = c.execute(
                'SELECT Problem, Solution FROM PROBLEM WHERE TextbookID=? AND ChapterID=? AND SectionID=? AND ExerciseID=?',
                values).fetchall()[0]
        elif current_exercise_type in (ExerciseType.DEFINITION, ExerciseType.THEOREM, ExerciseType.FACT):
            exercise_content = c.execute(
                'SELECT Front, Back FROM FACT WHERE TextbookID=? AND ChapterID=? AND SectionID=? AND ExerciseID=?',
                values).fetchall()[0]
        elif current_exercise_type is ExerciseType.LECTURE_TOPIC:
            exercise_content = c.execute(
                'SELECT Topic FROM LECTURE_TOPIC WHERE TextbookID=? AND ChapterID=? AND SectionID=? AND ExerciseID=?',
                values).fetchall()[0]
        else:
            raise ValueError('Other exercise types are currently not supported')

        if current_exercise_type is not ExerciseType.LECTURE_TOPIC:
            keys = ['problem', 'solution']
            exercise_content = {key: exercise_content[i] for (i, key) in enumerate(keys)}
        else:
            exercise_content = {'problem': exercise_content[0]}

        db.close()

        return exercise_content

    @staticmethod
    def get_exercise_type(textbook_id, chapter_id, section_id, exercise_id):
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        values = (textbook_id, chapter_id, section_id if section_id is not None else 'None', exercise_id)
        exercise_type = c.execute(
            'SELECT Type FROM EXERCISE WHERE TextbookID=? AND ChapterID=? AND SectionID=? AND ExerciseID=?',
            values).fetchone()
        try:
            current_exercise_type = ExerciseType(int(exercise_type[0]))
        except:
            pass

        db.close()
        return current_exercise_type

    @staticmethod
    def get_activated(textbook_id, chapter_id, section_id, exercise_id):
        db = sqlite3.connect('mathdb.sqlite')
        c = db.cursor()
        values = (textbook_id, chapter_id, section_id if section_id is not None else 'None', exercise_id)
        is_activated = c.execute(
            'SELECT IsActivated FROM EXERCISE WHERE TextbookID=? AND ChapterID=? AND SectionID=? AND ExerciseID=?',
            values).fetchone()[0]
        db.close()
        return bool(is_activated)
