import pandas as pd

from math import inf, isnan
from numpy import nan
from trainer import *
from itertools import chain, zip_longest


class Planner:
    """
    Planner takes the exercises, interactions, exercise settings, and mastery schedule and forms a list of exercises
    to practice that session for the current time. The each of the above items is a list of iterables. The order of
    which is from the database functions that provide that data. The exercises to practice at the current moment can
    be accessed by the session_problems attribute.

    Planner bins the exercises into either review, mastery, fail, or new. Each category has a specific requirement for
    when it is ready for study. Review, mastery, and fail exercises are then put into a group of 'old problems' with
    the 'new problems' coming from the new category. From this point the old problems and new problems are interleaved
    with a old problem coming first. The end result is an alternation between practice of old problems and new problems.
    """
    def __init__(self, textbook_exercises, exercise_interactions, exercise_settings, mastery_schedule):
        """

        :param textbook_exercises:
        :param exercise_interactions:
        :param exercise_settings:
        :param mastery_schedule:
        """
        self._master_schedule = pd.DataFrame(mastery_schedule)
        self._exercise_settings = pd.DataFrame(exercise_settings)

        columns = ['textbook', 'chapter', 'section', 'exercise', 'type']
        exercises = pd.DataFrame(textbook_exercises, columns=columns)

        columns = ['textbook', 'chapter', 'section', 'exercise', 'interaction', 'ending', 'duration', 'dateTime']
        interactions = pd.DataFrame(exercise_interactions, columns=columns)
        interactions = interactions.astype({'dateTime': 'datetime64'})

        self._exercises_df = exercises.merge(interactions, on=['textbook', 'chapter', 'section', 'exercise'], how='left')

        sorted_exercises = self._bucket_exercises()

        old_problems = sorted_exercises[(sorted_exercises.type == 'review') |
                                        (sorted_exercises.type == 'mastery') |
                                        (sorted_exercises.type == 'fail')]
        new_problems = sorted_exercises[sorted_exercises.type == 'new']

        # Alternate new problems from textbooks:
        textbook_indices = pd.unique(new_problems.textbook)
        new_indices = [new_problems[new_problems.textbook == index].index for index in textbook_indices]
        new_indices = chain(*zip_longest(*new_indices))
        new_indices = pd.DataFrame([x for x in new_indices if x is not None])
        # where problem is
        if len(new_problems):
            new_problems = new_problems.loc[new_indices[0]]


        # shuffle the old problems and interleave them with new problems to alternate between new exercises and review.
        old_problems = old_problems.sample(frac=1)
        old_problems.reset_index(inplace=True, drop=True)
        new_problems.reset_index(inplace=True, drop=True)
        old_problems.rename(index={x: 2 * x for x in old_problems.index}, inplace=True)
        new_problems.rename(index={x: 2 * x + 1 for x in new_problems.index}, inplace=True)

        session_problems = pd.concat([old_problems, new_problems])
        session_problems.sort_index(inplace=True)
        session_problems.drop('type', axis=1, inplace=True)
        self.session_problems = session_problems.values.tolist()

    def _is_review_ready(self, exercise):
        """
        Takes an exercise DataFrame with its interactions and returns True if the problem is ready to review today
        else it returns False.
        :return: A DataFrame with the exercise and its interactions
        """
        exercise_type_column_name = self._get_mastery_schedule_column(exercise)
        # this is because we don't have example master schedules in the database, I just treat it as a problem. 
        exercise_type_column_name = exercise_type_column_name if not 'example' else 'problem'
        time_between = self.days_since_last_interaction(exercise, InteractionType.SUCCESS)
        print(exercise_type_column_name)
        review_counter_in_days = self._exercise_settings.loc['review_counter', exercise_type_column_name]
        if time_between >= review_counter_in_days:
            return True

        return False

    def _is_mastery_ready(self, exercise):
        """
        Takes a mastery exercise and returns True if the exercise is ready for review for the current session otherwise
        returns False.
        :return: A problem that is under the exercise category.
        """
        exercise_type_column_name = self._get_mastery_schedule_column(exercise)
        # Following line is to account that problems and examples have the same mastery schedule.
        exercise_type_column_name = exercise_type_column_name if not "example" else 'problem'
        duration_since_practice = self.days_since_last_interaction(exercise, InteractionType.SUCCESS)

        most_recent_successes = self._get_most_recent_successes(exercise)

        if len(most_recent_successes) == 1:
            minimum_mastery_duration_in_days = self._master_schedule[exercise_type_column_name].min()
            if duration_since_practice >= minimum_mastery_duration_in_days:
                return True
        else:
            max_interaction_duration = self._get_best_recent_success_duration_in_days(exercise)
            exercise_type_mastery_schedule = self._master_schedule[exercise_type_column_name]
            non_achieved_durations = exercise_type_mastery_schedule[
                exercise_type_mastery_schedule > max_interaction_duration]
            minimum_not_achieved_in_days = non_achieved_durations.min()
            if duration_since_practice >= minimum_not_achieved_in_days:
                return True

        return False

    def _is_failed_ready(self, exercise):
        """
        Given a failed exercise returns True if the exercise is ready for practice otherwise returns False.
        :return: An exercise under the failed category.
        """
        exercise_type_column_name = self._get_mastery_schedule_column(exercise)
        # Following line is to account that problems and examples have the same mastery schedule.
        exercise_type_column_name = exercise_type_column_name if not "example" else 'problem'
        days_since_last_failure = self.days_since_last_interaction(exercise, InteractionType.FAILURE)
        minimum_duration_of_failure = days_since_last_failure
        failure_counter_in_days = self._exercise_settings.loc['failure_counter', exercise_type_column_name]
        if minimum_duration_of_failure >= failure_counter_in_days:
            return True

        return False


    @staticmethod
    def days_since_last_interaction(exercise, interaction_type):
        interactions = exercise.loc[exercise['ending'] == interaction_type.value]
        if interactions.empty:
            return inf

        time_of_last_review = interactions.tail(1)['dateTime'].values[0]
        now = pd.Timestamp.now()
        time_between = now - time_of_last_review
        time_between_in_days = time_between.days
        return time_between_in_days

    def _bucket_exercises(self):
        # todo play around with multiindex for chapter and exercise, I think that it might make things good
        study_exercises = self._exercises_df[['textbook', 'chapter', 'section', 'exercise']].drop_duplicates()
        study_exercises['type'] = nan


        for index, id in study_exercises.iterrows():
            current_exercise_bool_array = (self._exercises_df['chapter'] == id.chapter) & \
                                          (self._exercises_df['section'] == id.section) & \
                                          (self._exercises_df['exercise'] == id.exercise) & \
                                          (self._exercises_df['textbook'] == id.textbook)
            a_exercise = self._exercises_df.loc[current_exercise_bool_array]

            is_ready_for_study = False
            if self._review_eligible(a_exercise):
                category = 'review'
                if self._is_review_ready(a_exercise):
                    is_ready_for_study = True
            elif self._mastery_eligible(a_exercise):
                category = 'mastery'
                if self._is_mastery_ready(a_exercise):
                    is_ready_for_study = True
            elif self._fail_eligible(a_exercise):
                category = 'fail'
                if self._is_failed_ready(a_exercise):
                    is_ready_for_study = True
            elif self._new_eligible(a_exercise):
                category = 'new'
                is_ready_for_study = True
            else:
                print(a_exercise)
                raise ValueError('Exercise not considered apart of any group')


            if is_ready_for_study:
                study_exercises.loc[current_exercise_bool_array, 'type'] = category
            else:
                # The reason for the indexing at 0 is due to the fact that we're only dealing with one exercise at a
                # time.
                study_exercises.drop(study_exercises.index[(study_exercises['chapter'] == id.chapter) &
                                                           (study_exercises['section'] == id.section) &
                                                           (study_exercises['textbook'] == id.textbook) &
                                                           (study_exercises['exercise'] == id.exercise)], inplace=True)

            # The following is used for testing
            self._exercises_df.loc[current_exercise_bool_array, 'type'] = 'some shit'

        return study_exercises

    @staticmethod
    def _no_interactions(exercise):
        return isnan(exercise['interaction'].values[0])

    def _review_eligible(self, exercise):
        if self._no_interactions(exercise):
            return 0
        else:
            if len(exercise) == 1:
                return False

        exercise_type = self._get_mastery_schedule_column(exercise)
        max_interaction_duration = self._get_best_recent_success_duration_in_days(exercise)
        exercise_type = 'problem' if exercise_type == 'example' else exercise_type
        exercise_type = 'lecture' if exercise_type == 'lecture_topic' else exercise_type
        max_mastery_duration_in_days = self._master_schedule[exercise_type].max()
        return True if max_mastery_duration_in_days < max_interaction_duration else False

    def _get_best_recent_success_duration_in_days(self, exercise):
        most_recent_successes = self._get_most_recent_successes(exercise, False)

        duration_between_successes = most_recent_successes['dateTime'] - most_recent_successes['dateTime'].shift()
        max_interaction_duration = duration_between_successes.max()
        max_interaction_duration_in_days = max_interaction_duration.days
        return max_interaction_duration_in_days

    @staticmethod
    def _get_mastery_schedule_column(exercise):

        try:
            exercise_type = ExerciseType(int(exercise['type'].head(1)))
        except:
            pass
        column_name = exercise_type.name.lower()
        return column_name

    @staticmethod
    def _get_most_recent_successes(exercise, exclude_attempts=True):
        if exclude_attempts:
            exercise = exercise.loc[exercise['ending'] != InteractionType.ATTEMPT.value]

        failures = exercise.loc[(exercise['ending'] == InteractionType.FAILURE.value) ]

        if failures.empty:
            return exercise
        most_recent_failure = failures.tail(1).index[0]
        most_recent_successes = exercise[exercise.index > most_recent_failure]

        return most_recent_successes

    @staticmethod
    def _mastery_eligible(exercise):
        complete_interactions = exercise.loc[(exercise['ending'] == InteractionType.SUCCESS.value) |
                                             (exercise['ending'] == InteractionType.FAILURE.value)]

        if complete_interactions.empty:
            return False

        last_interaction = complete_interactions.tail(1)
        is_last_success = last_interaction['ending'].isin([InteractionType.SUCCESS.value]).all()

        recent_successes = Planner._get_recent_interactions(exercise)
        return is_last_success


    @staticmethod
    def _get_recent_interactions(exercise):
        """
        A helper function to get the interactions since the most recent complete interaction of an exercise.
        Where a complete interaction is either a failure or a success.
        :param exercise: A DataFrame for exercise.
        :return: A subset of exercise if there exists a complete interaction otherwise None
        """
        complete_interactions = exercise.loc[(exercise['ending'] == InteractionType.SUCCESS.value) |
                                             (exercise['ending'] == InteractionType.FAILURE.value)]

        last_interaction = complete_interactions.tail(1)
        recent_interactions = exercise[exercise.index > last_interaction.index[0]]
        return recent_interactions if not recent_interactions.empty else None

    @staticmethod
    def _fail_eligible(exercise):
        # If last interaction was a failure then the exercise is a failure.
        last_interaction = exercise.tail(1)
        last_interaction_was_failure = last_interaction['ending'].isin([InteractionType.FAILURE.value]).all()
        if last_interaction_was_failure:
            return True

        return False

    @staticmethod
    def _new_eligible(exercise):
        # No interactions whatsoever is new.
        if exercise['ending'].isna().all():
            return True

        # Check if exercise has any interactions that are not ATTEMPT.
        is_only_attempts = exercise['ending'].isin([InteractionType.ATTEMPT.value]).all()
        if not is_only_attempts:
            # Has an interaction that is not ATTEMPT.
            return False

        return True
