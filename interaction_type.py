from enum import Enum


class InteractionType(Enum):
    FAILURE = -1
    ATTEMPT = 0
    SUCCESS = 1
