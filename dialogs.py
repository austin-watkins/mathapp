from PyQt5.QtWidgets import *


class NewTextbookDialog(QDialog):
    def __init__(self, parent=None):
        super(NewTextbookDialog, self).__init__(parent)

        # Dialog Properties:
        self.setMinimumHeight(340)
        self.setMinimumWidth(540)
        self.setWindowTitle("Textbook Settings")

        # Widgets:
        width_label = QLabel("Textbook title:")
        self.textbook_title_input = QLineEdit()
        width_label.setBuddy(self.textbook_title_input)

        topics_label = QLabel("Topics:")
        self.textbook_topics_input = QLineEdit()
        topics_label.setBuddy(self.textbook_topics_input)

        problems_per_day_label = QLabel("Problems per day:")
        self.problems_per_day_comboBox = QSpinBox()
        problems_per_day_label.setBuddy(self.problems_per_day_comboBox)

        days_to_read_section_label = QLabel("Days to read section")
        self.days_to_read_section_spinBox = QSpinBox()
        days_to_read_section_label.setBuddy(self.days_to_read_section_spinBox)

        forced_attempts_label = QLabel("Forced attempts:")
        self.forced_attempts_comboBox = QSpinBox()
        forced_attempts_label.setBuddy(self.forced_attempts_comboBox)

        expected_finish_date_label = QLabel("Expected finish date")
        self.expected_finish_date_input = QLineEdit()
        self.expected_finish_date_input.setReadOnly(True)
        expected_finish_date_label.setBuddy(self.expected_finish_date_input)

        toolbox = QToolBox()
        self.exercise_schedule_problems = self.ExerciseScheduleWidget(self)
        toolbox.addItem(self.exercise_schedule_problems, "Problems, facts, proofs, and lectures")
        self.exercise_schedule_core = self.ExerciseScheduleWidget(self)
        toolbox.addItem(self.exercise_schedule_core, "Definitions, and theorems")

        self.section_problems_checkbox = QCheckBox("Section problems")
        self.chapter_problems_checkbox = QCheckBox("Chapter problems")

        dialog_buttons = QHBoxLayout()
        save_button = QPushButton("Save")
        cancel_button = QPushButton("Cancel")
        dialog_buttons.addWidget(save_button)
        dialog_buttons.addWidget(cancel_button)

        # Layout:
        layout = QGridLayout()
        layout.addWidget(width_label, 0, 0)
        layout.addWidget(self.textbook_title_input, 0, 1)
        layout.addWidget(topics_label, 1, 0)
        layout.addWidget(self.textbook_topics_input, 1, 1)
        layout.addWidget(problems_per_day_label, 2, 0)
        layout.addWidget(self.problems_per_day_comboBox, 2, 1)
        layout.addWidget(days_to_read_section_label, 3, 0)
        layout.addWidget(self.days_to_read_section_spinBox, 3, 1)
        layout.addWidget(forced_attempts_label, 4, 0)
        layout.addWidget(self.forced_attempts_comboBox, 4, 1)
        layout.addWidget(expected_finish_date_label, 5, 0)
        layout.addWidget(self.expected_finish_date_input, 5, 1)
        layout.addWidget(toolbox, 0, 3, 4, 2)
        layout.addWidget(self.section_problems_checkbox, 4, 3)
        layout.addWidget(self.chapter_problems_checkbox, 5, 3)
        layout.addItem(dialog_buttons, 6, 3)
        self.setLayout(layout)

        # Connections:
        save_button.clicked.connect(self.accept)
        cancel_button.clicked.connect(self.reject)

    class ExerciseScheduleWidget(QWidget):
        def __init__(self, schedule_parent=None):
            super(NewTextbookDialog.ExerciseScheduleWidget, self).__init__(schedule_parent)

            # Widgets:
            review_counter_label = QLabel("Review counter:")
            self.review_counter_spinBox = QSpinBox()
            review_counter_label.setBuddy(self.review_counter_spinBox)

            failure_counter_label = QLabel("Failure counter:")
            self.failure_counter_spinBox = QSpinBox()
            failure_counter_label.setBuddy(self.failure_counter_spinBox)

            mastery_schedule_label = QLabel("Mastery schedule")
            self.mastery_schedule_input = QLineEdit()
            mastery_schedule_label.setBuddy(self.mastery_schedule_input)

            # Layout:
            schedule_layout = QGridLayout()
            schedule_layout.addWidget(review_counter_label, 0, 0)
            schedule_layout.addWidget(self.review_counter_spinBox, 0, 1)
            schedule_layout.addWidget(failure_counter_label, 1, 0)
            schedule_layout.addWidget(self.failure_counter_spinBox, 1, 1)
            schedule_layout.addWidget(mastery_schedule_label, 2, 0)
            schedule_layout.addWidget(self.mastery_schedule_input, 2, 1)

            self.setLayout(schedule_layout)

    @staticmethod
    def get_values_textbook_dialog(dialog):
        textbook_values = {'ForcedAttempts': dialog.forced_attempts_comboBox.value(),
                           'Name': dialog.textbook_title_input.text(),
                           'ProblemsPerDay': dialog.problems_per_day_comboBox.value(),
                           'DaysToReadSection': dialog.days_to_read_section_spinBox.value(),
                           'HasSectionProblems': dialog.section_problems_checkbox.checkState(),
                           'HasChapterProblems': dialog.chapter_problems_checkbox.checkState(),
                           'TextbookTopics': dialog.textbook_topics_input.text()}
        problem_review_counter = dialog.exercise_schedule_problems.review_counter_spinBox.value()
        problem_failure_counter = dialog.exercise_schedule_problems.failure_counter_spinBox.value()
        core_review_counter = dialog.exercise_schedule_core.review_counter_spinBox.value()
        core_failure_counter = dialog.exercise_schedule_core.failure_counter_spinBox.value()
        exercise_settings_values = {'problem_review_counter': problem_review_counter,
                                    'problem_failure_counter': problem_failure_counter,
                                    'core_review_counter': core_review_counter,
                                    'core_failure_counter': core_failure_counter}
        schedule_duration_exercise = dialog.exercise_schedule_problems.mastery_schedule_input.text().split(',')
        schedule_duration_core = dialog.exercise_schedule_core.mastery_schedule_input.text().split(',')
        mastery_schedule = {'schedule_duration_exercise': schedule_duration_exercise,
                            'schedule_duration_core': schedule_duration_core}
        return {'textbook_values': textbook_values,
                'exercise_settings_values': exercise_settings_values,
                'mastery_schedule_values': mastery_schedule}


class NewChapterDialog(QDialog):
    def __init__(self, parent=None):
        super(NewChapterDialog, self).__init__(parent)

        # Widgets:
        name_label = QLabel("Name:")
        self.name_input = QLineEdit()
        name_label.setBuddy(self.name_input)

        number_of_problems_label = QLabel("Number of Problems: ")
        self.number_of_problems_input = QSpinBox()

        cancel_button = QPushButton("Cancel")
        save_button = QPushButton("Save")

        # Layout:
        layout = QGridLayout()
        layout.addWidget(name_label, 0, 0)
        layout.addWidget(self.name_input, 0, 1)
        layout.addWidget(number_of_problems_label, 1, 0)
        layout.addWidget(self.number_of_problems_input, 1, 1)
        layout.addWidget(save_button, 2, 0)
        layout.addWidget(cancel_button, 2, 1)
        self.setLayout(layout)

        # Connections:
        cancel_button.clicked.connect(self.reject)
        save_button.clicked.connect(self.accept)

    @staticmethod
    def get_values(dialog):
        return {'name': dialog.name_input.text(),
                'number_of_problems': dialog.number_of_problems_input.value()}




