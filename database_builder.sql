-- we don't know how to generate root <with-no-name> (class Root) :(
create table TEXTBOOK
(
	TextbookID integer not null
		constraint TEXTBOOK_pk
			primary key,
	CompletionDate text not null,
	ForcedAttempts integer default 0 not null,
	Name text not null,
	Note text,
	Cover text,
	ProblemsPerDay integer not null,
	DaysToReadSection integer not null,
	HasSectionProblems integer not null,
	HasChapterProblems integer not null
);

create table CHAPTER
(
	TextbookID int not null
		constraint MASTERY_SCHEDULE_TEXTBOOK_TextbookID_fk
			references TEXTBOOK
				on update cascade on delete cascade,
	ChapterID int not null,
	Name text not null,
	ProblemCount int not null,
	Note text,
	constraint MASTERY_SCHEDULE_pk
		primary key (TextbookID, ChapterID)
);

create table EXERCISE_SETTINGS
(
	TextbookID int not null
		constraint EXERCISE_SETTINGS_TEXTBOOK_TextbookID_fk
			references TEXTBOOK
				on update cascade on delete cascade,
	Type text not null,
	ReviewCounter int not null,
	FailureCounter int not null,
	constraint EXERCISE_SETTINGS_pk
		primary key (TextbookID, Type)
);

create table MASTERY_SCHEDULE
(
	TextbookID integer not null
		constraint EXERCISE_SCHEDULE_TEXTBOOK_TextbookID_fk
			references TEXTBOOK
				on update cascade on delete cascade,
	Type text not null,
	Step integer not null,
	Duration integer not null,
	constraint EXERCISE_SCHEDULE_pk
		primary key (TextbookID, Type, Step)
);

create table SECTION
(
	TextbookID int not null
		constraint SECTION_TEXTBOOK_TextbookID_fk
			references TEXTBOOK
				on update cascade on delete cascade,
	ChapterID int not null
		constraint SECTION_CHAPTER_ChapterID_fk
			references CHAPTER (ChapterID)
				on update cascade on delete cascade,
	SectionID int not null,
	Name text not null,
	ProblemCount int not null,
	Note text,
	HasRead int not null,
	constraint SECTION_pk
		primary key (TextbookID, ChapterID, SectionID)
);

create table EXERCISE
(
	TextbookID int not null
		constraint EXERCISE_TEXTBOOK_TextbookID_fk
			references TEXTBOOK
				on update cascade on delete cascade,
	ChapterID int not null
		constraint EXERCISE_CHAPTER_ChapterID_fk
			references CHAPTER (ChapterID)
				on update cascade on delete cascade,
	SectionID int not null
		constraint EXERCISE_SECTION_SectionID_fk
			references SECTION (SectionID)
				on update cascade on delete cascade,
	ExerciseID int not null,
	Type text not null,
	Note text,
	constraint EXERCISE_pk
		primary key (TextbookID, ChapterID, SectionID, ExerciseID)
);

create table ALTERNATE_SOLUTION
(
	TextbookID int not null
		constraint ALTERNATE_SOLUTION_TEXTBOOK_TextbookID_fk
			references TEXTBOOK
				on update cascade on delete cascade,
	ChapterID int not null
		constraint ALTERNATE_SOLUTION_CHAPTER_ChapterID_fk
			references CHAPTER (ChapterID)
				on update cascade on delete cascade,
	SectionID int not null
		constraint ALTERNATE_SOLUTION_SECTION_SectionID_fk
			references SECTION (SectionID)
				on update cascade on delete cascade,
	ExerciseID int not null
		constraint ALTERNATE_SOLUTION_EXERCISE_ExerciseID_fk
			references EXERCISE (ExerciseID)
				on update cascade on delete cascade,
	Solution text not null,
	constraint ALTERNATE_SOLUTION_pk
		primary key (TextbookID, ChapterID, SectionID, ExerciseID)
);

create table FACT
(
	TextbookID int not null
		constraint FACT_CHAPTER_TextbookID_fk
			references CHAPTER (TextbookID)
				on update cascade on delete cascade,
	ChapterID int not null
		constraint FACT_CHAPTER_ChapterID_fk
			references CHAPTER (ChapterID)
				on update cascade on delete cascade,
	SectionID int not null
		constraint FACT_SECTION_SectionID_fk
			references SECTION (SectionID)
				on update cascade on delete cascade,
	ExerciseID int not null
		constraint FACT_EXERCISE_ExerciseID_fk
			references EXERCISE (ExerciseID)
				on update cascade on delete cascade,
	Front int not null,
	Back int not null,
	Hint int,
	constraint FACT_pk
		primary key (TextbookID, ChapterID, SectionID, ExerciseiD)
);

create table INTERACTION
(
	TextbookID int not null
		constraint INTERACTION_TEXTBOOK_TextbookID_fk
			references TEXTBOOK
				on update cascade on delete cascade,
	ChapterID int not null
		constraint INTERACTION_CHAPTER_ChapterID_fk
			references CHAPTER (ChapterID)
				on update cascade on delete cascade,
	SectionID int not null
		constraint INTERACTION_SECTION_SectionID_fk
			references SECTION (SectionID)
				on update cascade on delete cascade,
	ExerciseID int not null
		constraint INTERACTION_EXERCISE_ExerciseID_fk
			references EXERCISE (ExerciseID)
				on update cascade on delete cascade,
	InteractionNumber int not null,
	Ending int not null,
	Duration int not null,
	HintActivated int not null,
	Date text not null,
	constraint INTERACTION_pk
		primary key (TextbookID, ChapterID, SectionID, ExerciseID, InteractionNumber)
);

create table LECTURE_TOPIC
(
	TextbookID int not null
		constraint LECTURE_TOPIC_TEXTBOOK_TextbookID_fk
			references TEXTBOOK
				on update cascade on delete cascade,
	ChapterID int not null
		constraint LECTURE_TOPIC_CHAPTER_ChapterID_fk
			references CHAPTER (ChapterID)
				on update cascade on delete cascade,
	SectionID int not null
		constraint LECTURE_TOPIC_SECTION_SectionID_fk
			references SECTION (SectionID)
				on update cascade on delete cascade,
	ExerciseID int not null
		constraint LECTURE_TOPIC_EXERCISE_ExerciseID_fk
			references EXERCISE (ExerciseID)
				on update cascade on delete cascade,
	Topic text not null,
	constraint LECTURE_TOPIC_pk
		primary key (TextbookID, ChapterID, SectionID, ExerciseID)
);

create table PRIOR_LECTURE
(
	TextbookID int not null
		constraint PRIOR_LECTURE_TEXTBOOK_TextbookID_fk
			references TEXTBOOK
				on update cascade on delete cascade,
	ChapterID int not null
		constraint PRIOR_LECTURE_CHAPTER_ChapterID_fk
			references CHAPTER (ChapterID)
				on update cascade on delete cascade,
	SectionID int not null
		constraint PRIOR_LECTURE_SECTION_SectionID_fk
			references SECTION (SectionID)
				on update cascade on delete cascade,
	ExerciseID int
		constraint PRIOR_LECTURE_EXERCISE_ExerciseID_fk
			references EXERCISE (ExerciseID)
				on update cascade on delete cascade,
	PriorLecture int not null,
	constraint PRIOR_LECTURE_pk
		primary key (TextbookID, ChapterID, SectionID, ExerciseID)
);

create table PROBLEM
(
	TextbookID int not null
		constraint PROBLEM_TEXTBOOK_TextbookID_fk
			references TEXTBOOK
				on update cascade on delete cascade,
	ChapterID int not null
		constraint PROBLEM_CHAPTER_ChapterID_fk
			references CHAPTER (ChapterID)
				on update cascade on delete cascade,
	SectionID int not null
		constraint PROBLEM_SECTION_SectionID_fk
			references SECTION (SectionID)
				on update cascade on delete cascade,
	ExerciseID int not null
		constraint PROBLEM_EXERCISE_ExerciseID_fk
			references EXERCISE (ExerciseID)
				on update cascade on delete cascade,
	Hint text,
	Problem text not null,
	Instruction text,
	IsDifficult int default 0 not null,
	Solution text,
	IsHomework int default 0 not null,
	constraint PROBLEM_pk
		primary key (TextbookID, ChapterID, SectionID, ExerciseID)
);

create unique index TEXTBOOK_TextbookID_uindex
	on TEXTBOOK (TextbookID);

create table TEXTBOOK_TOPIC
(
	TextbookID int not null
		constraint TextbookID
			references TEXTBOOK
				on update cascade on delete cascade,
	Topic text,
	constraint TEXTBOOK_TOPIC_pk
		primary key (TextbookID, Topic)
);

create table THEOREM
(
	TextbookID int not null
		constraint THEOREM_TEXTBOOK_TextbookID_fk
			references TEXTBOOK
				on update cascade on delete cascade,
	ChapterID int not null
		constraint THEOREM_CHAPTER_ChapterID_fk
			references CHAPTER (ChapterID)
				on update cascade on delete cascade,
	SectionID int not null
		constraint THEOREM_SECTION_SectionID_fk
			references SECTION (SectionID)
				on update cascade on delete cascade,
	ExerciseID int not null
		constraint THEOREM_EXERCISE_ExerciseID_fk
			references EXERCISE (ExerciseID)
				on update cascade on delete cascade,
	Name text not null,
	constraint THEOREM_pk
		primary key (TextbookID, ChapterID, SectionID, ExerciseID)
);
